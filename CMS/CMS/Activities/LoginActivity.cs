﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using RestSharp;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace CMS.Activities
{
    [Activity(Label = "Credit Managment Login", MainLauncher = false, Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {
        //int count = 1;
         Button button, button2,button3,button4;
        EditText username, pass;
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Login);
          button = FindViewById<Button>(Resource.Id.btnLogin);
            button2 = FindViewById<Button>(Resource.Id.btnClear);
            button3 = FindViewById<Button>(Resource.Id.btnReg);
            button4 = FindViewById<Button>(Resource.Id.btnForg);
            username = FindViewById<EditText>(Resource.Id.username);
           pass = FindViewById<EditText>(Resource.Id.password);
            baseUrl = "http://169.239.27.37/CMSApp";

            button2.Click += delegate
            {

                username.Text = "";
                pass.Text = "";
            };

            button3.Click += delegate
            {
                StartActivity(typeof(RegisterActivity));
            };

            button4.Click += delegate
            {
                StartActivity(typeof(ForgotActivity));
            };
            button.Click += LoginClick;
        }

        public void LoginClick(object sender, EventArgs e)
        {
            string responJsonText = "";
            bool validA = username.Text.Replace(" ", "").All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));

            if (username.Text == "")
            {
                Toast.MakeText(this, "Username is required", ToastLength.Long).Show();
                return;
            }else if (validA==false)
            {
                Toast.MakeText(this, "Username must be a letter , digit or underscore", ToastLength.Long).Show();
                return;
            }
            else if (pass.Text == "")
            {
                Toast.MakeText(this, "Password is required", ToastLength.Long).Show();
                return;
            }


            var client = new RestClient(baseUrl);
            var request = new RestRequest("Login/{s}/{p}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("s", username.Text.Replace(" ", ""));
            request.AddUrlSegment("p", ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;


            if (validate == "0")
            {
                Toast.MakeText(this, "Wrong details please try again", ToastLength.Long).Show();
                return;
            }
            else if (validate == "1")
            {
                var client2 = new RestClient(baseUrl);
                var request2 = new RestRequest("LoginMem/{s}/{p}", Method.GET);
                request2.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request2.AddUrlSegment("s", username.Text.Replace(" ", ""));
                request2.AddUrlSegment("p", ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));
                IRestResponse response2 = client2.Execute(request2);
                string validate2 = response2.Content;

                validate2 = validate2.Replace(@"""", "");
                string val2 = validate2;

                Toast.MakeText(this, "You are logged in", ToastLength.Long).Show();
                Finish();
                var activity2 = new Intent(this, typeof(MainActivity));
                activity2.PutExtra("Member", val2);
                activity2.PutExtra("Branch", "Lusaka");
                activity2.PutExtra("Username", username.Text);
                StartActivity(activity2);

                return;
            }

        }

         public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }




    }


        }
    



