using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;

using CMS.Fragments;
using Android.Support.V7.App;
using Android.Support.V4.View;
using Android.Support.Design.Widget;

namespace CMS.Activities
{
    [Activity(Label = "GOODFELLOW FIN", MainLauncher =false, LaunchMode = LaunchMode.SingleTop, Icon = "@drawable/Icon")]
    public class MainActivity : BaseActivity
    {

        DrawerLayout drawerLayout;
        NavigationView navigationView;

        public static string membernumber { get; set; }
        public static string branch { get; set; }
        public static string username { get; set; }

        public static string loanno { get; set; }
        public static decimal loanbalance { get; set; }

        public static string baseUrl { get; set; }
        TextView jump;
        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.main;
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            membernumber = "";
            branch = "";
            username = "";
           membernumber = Intent.GetStringExtra("Member") ?? "Data not available";
           branch = Intent.GetStringExtra("Branch") ?? "Data not available";
            username = Intent.GetStringExtra("Username") ?? "Data not available";
            drawerLayout = this.FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            jump = this.FindViewById<TextView>(Resource.Id.lender);
            jump.Text = "Client Number :"+membernumber;
            baseUrl = "http://166.63.10.232/CMSApp";
            //Set hamburger items menu
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            //setup navigation view
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //handle navigation
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.home:
                        ListItemClicked(0);
                        break;
                    case Resource.Id.La:
                        ListItemClicked(1);
                        break;
                    case Resource.Id.ls:
                        ListItemClicked(2);
                        break;
                    case Resource.Id.cp:
                        ListItemClicked(3);
                        break;
                    case Resource.Id.outs:
                        ListItemClicked(4);
                        break;
                }

                try
                {
                    Snackbar.Make(drawerLayout, "You selected: " + e.MenuItem.TitleFormatted, Snackbar.LengthLong)
                                .Show();
                }
                catch (System.Exception)
                {
                    
                }
                if (e.MenuItem.TitleFormatted.ToString() == "Home")
                {
                    Android.Support.V4.App.Fragment fragment = null;
                    SupportFragmentManager.BeginTransaction()
                      .Replace(Resource.Id.content_frame, Fragment1.NewInstance())
                      .Commit();
                }
               
                drawerLayout.CloseDrawers();
            };


            //if first time you will want to go ahead and click first item.
            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
              
                    ListItemClicked(0);
                    Android.Support.V4.App.Fragment fragment = null;
                    fragment = Fragment1.NewInstance();
                    SupportFragmentManager.BeginTransaction()
                   .Replace(Resource.Id.content_frame, fragment)
                   .Commit();
               
            }
        }

        int oldPosition = -1;
        private void ListItemClicked(int position)
        {
            //this way we don't load twice, but you might want to modify this a bit.
            if (position == oldPosition)
                return;

            oldPosition = position;

            Android.Support.V4.App.Fragment fragment = null;
            switch (position)
            {
                case 0:
                    fragment = Fragment1.NewInstance();
                    break;
                case 1:
                    fragment = Borrower.NewInstance();
                    break;
                case 2:
                    fragment = Loans.NewInstance();
                    break;
                case 3:
                  
                    fragment = ChangePassword.NewInstance();
                    
                    break;
                case 4:
                    {
                        Finish();
                        StartActivity(typeof(LoginActivity));
                    }
                    break;
            }

            try
            {
                SupportFragmentManager.BeginTransaction()
                   .Replace(Resource.Id.content_frame, fragment).Detach(fragment).Attach(fragment)
                   .Commit();
            }
            catch (System.Exception)
            {

            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}

