﻿using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using static Android.Widget.TabHost;

namespace CMS.Fragments
{
    public class ApplicationForm : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }


        public static ApplicationForm NewInstance()
        {
            var frag2 = new ApplicationForm { Arguments = new Bundle() };
            return frag2;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.Appl, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Guarantor Information");
            spec1.SetContent(Resource.Id.tab1);
            spec1.SetIndicator("Guarantor Information");

            TabSpec spec2 = tabHost.NewTabSpec("Collateral Offered");
            spec2.SetIndicator("Collateral Offered");
            spec2.SetContent(Resource.Id.tab2);

            TabSpec spec3 = tabHost.NewTabSpec("Other Loans");
            spec3.SetIndicator("Other Loans");
            spec3.SetContent(Resource.Id.tab3);

            TabSpec spec4 = tabHost.NewTabSpec("Attach Documents");
            spec4.SetIndicator("Attach Documents");
            spec4.SetContent(Resource.Id.tab4);

            TabSpec spec5 = tabHost.NewTabSpec("Financial Requirements");
            spec5.SetIndicator("Financial Requirements");
            spec5.SetContent(Resource.Id.tab5);

            tabHost.AddTab(spec1);
            tabHost.AddTab(spec2);
            tabHost.AddTab(spec3);
            tabHost.AddTab(spec4);
            tabHost.AddTab(spec5);

           
            return view;
        }
    }
}