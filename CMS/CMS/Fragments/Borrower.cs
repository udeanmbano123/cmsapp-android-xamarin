using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using CMS.Activities;
using CMS.Model;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Extensions.MonoHttp;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Fragments
{
    public class Borrower : Fragment
    {
        

        private List<string> products = new List<string>();
        Spinner sbu;
        EditText membernumber, loanamt;
        Button submit;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Borrower NewInstance()
        {
            var frag1 = new Borrower { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.BorrowerAppl, null);
            submit = (Button)view.FindViewById(Resource.Id.btnLogin);
            loanamt = (EditText)view.FindViewById(Resource.Id.loanamount);
            membernumber = (EditText)view.FindViewById(Resource.Id.member);
            sbu = (Spinner)view.FindViewById(Resource.Id.sbu);
            products.Add("--Select Product--");
            try
            {
                var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("Products", Method.GET);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                IRestResponse response = client.Execute(request);
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<Products> dataList = JsonConvert.DeserializeObject<List<Products>>(validate);

                var dbsel = from s in dataList
                            select s;

                foreach (var p in dbsel)
                {
                    products.Add(p.DisplayName);
                }

            }
            catch (System.Exception)
            {


            }

            membernumber.Text = MainActivity.membernumber;
            var adapter1 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, products);

            sbu.Adapter = adapter1;

            submit.Click += Submit_Click;

            return view;
        }

        private void Submit_Click(object sender, System.EventArgs e)
        {
            if (loanamt.Text == "")
            {
                Toast.MakeText(this.Activity, "Loan amount  is required", ToastLength.Short).Show();
                return;
                
            }
            else if (sbu.SelectedItem.ToString() == "--Select Product--")
            {
                Toast.MakeText(this.Activity, "Product is required", ToastLength.Short).Show();
                return;

            }
            else
            {

                submit.Enabled = false;
               
                Toast.MakeText(this.Activity, "Please be patient while processing request", ToastLength.Short).Show();
                
                //string emls = eml.Text;
                //string cells = (cell.Text).Replace("+", "%20");
                //string names = HttpUtility.UrlEncode(name.Text).Replace("+", "%20");
                var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("LoanApplication/{customernumber}/{loanamount}/{product}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("customernumber", MainActivity.membernumber);
                request.AddUrlSegment("loanamount", loanamt.Text);
                request.AddUrlSegment("product", sbu.SelectedItem.ToString().Replace(" ", ""));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                string val = validate;
                if (val == "Successful")
                {
                    //Toast.MakeText(this.Activity, "The loan application was succesful", ToastLength.Short).Show();
                    loanamt.Text = "";
                     Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this.Activity);
                    Android.App.AlertDialog alertDialog = builder.Create();
                    alertDialog.SetTitle("Notification");
                    alertDialog.SetIcon(Resource.Drawable.ic_launcher);
                    alertDialog.SetMessage("The loan application was successful");


                    alertDialog.SetButton("OK", (s, ev) =>
                    {
                        //DO Something
                        submit.Enabled = true;
                        loanamt.Text = "0";
                        products.Clear();
                        products.Add("--Select Product--");
                        try
                        {
                            var client2 = new RestClient(MainActivity.baseUrl);
                            var request2 = new RestRequest("Products", Method.GET);
                            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                            IRestResponse response2 = client2.Execute(request2);
                            // request.AddBody(new tblMeetings {  });
                            string validate2 = response2.Content;


                            List<Products> dataList = JsonConvert.DeserializeObject<List<Products>>(validate2);

                            var dbsel = from p in dataList
                                        select p;

                            foreach (var u in dbsel)
                            {
                                products.Add(u.DisplayName);
                            }
                            var adapter1 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, products);

                            sbu.Adapter = adapter1;
                        }
                        catch (System.Exception)
                        {


                        }

                    });

                    alertDialog.Show();
                }
                else
                {
                    //Toast.MakeText(this.Activity, "The loan application was unsuccessful", ToastLength.Short).Show();
                    loanamt.Text = "0";

                    Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this.Activity);
                    Android.App.AlertDialog alertDialog = builder.Create();
                    alertDialog.SetTitle("Notification");
                    alertDialog.SetIcon(Resource.Drawable.ic_launcher);
                    alertDialog.SetMessage(val);


                    alertDialog.SetButton("OK", (s, ev) =>
                    {
                        //DO Something
                        submit.Enabled = true;

                    });
                    alertDialog.Show();
                }



            }
        }
    }
}