using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using CMS.Model;
using CMS;

namespace OldMutualAndroid.Model
{
    class CustomListS : BaseAdapter
    {
        private Activity context;
        public List<LoanStatement> listitem;
      
        private Drawable[] imageId;
        //public override int Count
        //{
        //    get
        //    {
        //        return listitem.Count;
        //    }
        //}
    
        public CustomListS(Activity context, List<LoanStatement> listitem,Drawable[] imageId)
        {
            this.context = context;
            this.listitem = listitem;
  

            this.imageId = imageId;
        }
        public override int Count
        {
            get { return listitem.Count; }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public LoanStatement GetItemAtPosition(int position)
        {
            return listitem[position];
        }

        public String GetItemAtPositionId(int position)
        {
            return listitem[position].Refrence.ToString();
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listitem[position];
            var view = context.LayoutInflater.Inflate(Resource.Layout.list_singleS, parent, false);
            TextView txtTitle = (TextView)view.FindViewById(Resource.Id.txt4);
            TextView txtStatus= (TextView)view.FindViewById(Resource.Id.txt2);
            TextView txtAmount = (TextView)view.FindViewById(Resource.Id.txt3);
            TextView txtDesc = (TextView)view.FindViewById(Resource.Id.txt);
            TextView txtBal = (TextView)view.FindViewById(Resource.Id.txt7);
            ImageView imageView = (ImageView)view.FindViewById(Resource.Id.img);
            txtTitle.SetText("LAST TRANSACTION DATE :"+ item.TrxnDate.ToString("dd-MMM-yyyy"), TextView.BufferType.Normal);
            txtAmount.SetText("AMOUNT : ZMW "+item.Amount.ToString("#,##0.00"), TextView.BufferType.Normal);
            txtStatus.SetText("LOAN ID  :" + item.Refrence, TextView.BufferType.Normal);
            txtDesc.SetText("DESCRIPTION :"+item.Description, TextView.BufferType.Normal);
            txtBal.SetText("CUMULATIVE BALANCE : ZMW " + item.Balance.ToString("#,##0.00"), TextView.BufferType.Normal);
            imageView.SetImageDrawable(imageId[position]);
            return view;
        }
    }
}