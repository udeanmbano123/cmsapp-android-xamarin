﻿using Android.App;
using Android.OS;
using Android.Widget;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;

namespace CMS.Activities
{
    [Activity(Label = "Forgot Password", MainLauncher = false, Icon = "@drawable/icon")]
    public class ForgotActivity : Activity
    {
        //int count = 1;
        Button btn, btn2;
        EditText eml;
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Forgot);
            btn = FindViewById<Button>(Resource.Id.btnLogin);
            btn2 = FindViewById<Button>(Resource.Id.btnBack);
            eml = FindViewById<EditText>(Resource.Id.email);
            baseUrl = "http://166.63.10.232/CMSApp";
            btn2.Click += delegate
            {
                StartActivity(typeof(LoginActivity));
            };

            btn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            bool validA = eml.Text.All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));

            if (eml.Text=="")
            {
                Toast.MakeText(this, "Client Number is required", ToastLength.Short).Show();
                return;
            }
           else
            {
              
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Forg/{s}", Method.PUT);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s", eml.Text.ToString().Replace("*", "-"));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                string val = validate;

               

                Toast.MakeText(this, val, ToastLength.Short).Show();
                eml.Text = "";
                return;
            }


        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);
           
            return BitConverter.ToString(hashedBytes);
        }
 
    }
    }


