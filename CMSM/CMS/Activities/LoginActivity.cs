﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using RestSharp;
using System;
using System.Linq;
using System.Security.Cryptography;
using SQLite;
using CMS.Model;
using System.IO;

namespace CMS.Activities
{
    [Activity(Label = "Credit Managment Staff Login", MainLauncher = false, Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {
        //int count = 1;
         Button button, button2,button3,button4;
        EditText username, pass;
        public string path;
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            //create database
            path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");
           string nme= createDatabase(path);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Login);
          button = FindViewById<Button>(Resource.Id.btnLogin);
            button2 = FindViewById<Button>(Resource.Id.btnClear);
            button3 = FindViewById<Button>(Resource.Id.btnReg);
            button4 = FindViewById<Button>(Resource.Id.btnForg);
            username = FindViewById<EditText>(Resource.Id.username);
           pass = FindViewById<EditText>(Resource.Id.password);
            baseUrl = "http://166.63.10.232/CMSSApi";

            button2.Click += delegate
            {

                username.Text = "";
                pass.Text = "";
            };

            button3.Click += delegate
            {
                StartActivity(typeof(RegisterActivity));
            };

            button4.Click += delegate
            {
                StartActivity(typeof(ForgotActivity));
            };
            button.Click += LoginClick;
        }

        public void LoginClick(object sender, EventArgs e)
        {
            string responJsonText = "";
            bool validA = username.Text.Replace(" ", "").All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));

            if (username.Text == "")
            {
                Toast.MakeText(this, "Username is required", ToastLength.Long).Show();
                return;
            }else if (validA==false)
            {
                Toast.MakeText(this, "Username must be a letter , digit or underscore", ToastLength.Long).Show();
                return;
            }
            else if (pass.Text == "")
            {
                Toast.MakeText(this, "Password is required", ToastLength.Long).Show();
                return;
            }
            else
            {
                int my = findNumberRecords(path, username.Text.Replace(" ", ""), ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));


                if (my==1)
                {
                    Toast.MakeText(this, "You are logged in.Online verification will be done", ToastLength.Long).Show();
                    Finish();
                    path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");


                    string myc = findNumberRecordsS(path, username.Text.Replace(" ", ""), ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));
                   // usertypes.Add("r");
                   // usertypes.Add("Loan Officer");
                    if (myc.Replace(" ", "").ToLower() == "loanapprover")
                    {
                    var activity2 = new Intent(this, typeof(ApproverActivity));
                    activity2.PutExtra("Member",myc);
                    activity2.PutExtra("Branch", "Lusaka");
                    activity2.PutExtra("Username", username.Text);
                    
                    StartActivity(activity2);
                    }else if(myc.Replace(" ", "").ToLower() == "loanofficer")
                    {
                        var activity2 = new Intent(this, typeof(MainActivity));
                        activity2.PutExtra("Member",myc);
                        activity2.PutExtra("Branch", "Lusaka");
                        activity2.PutExtra("Username", username.Text);
                   
                        StartActivity(activity2);
                    }

                   
                    return;
                }else
                {
                    Toast.MakeText(this, "Wrong details please try again", ToastLength.Long).Show();
                    return;
                }
            }

        }
        private int findNumberRecords(string path,String usern,String pass)
        {
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                var count = db.ExecuteScalar<int>("SELECT Count(*) FROM User where username='"+ usern + "' and Password='" + pass + "'");

                // for a non-parameterless query
                // var count = db.ExecuteScalar<int>("SELECT Count(*) FROM Person WHERE FirstName="Amy");

                return count;
            }
            catch (SQLiteException ex)
            {
                return -1;
            }
        }
        private string findNumberRecordsS(string path, String usern, String pass)
        {
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                var count = db.ExecuteScalar<string>("SELECT usertype FROM User where username='" + usern + "' and Password='" + pass + "'");

             
                return count;
            }
            catch (SQLiteException ex)
            {
                return "None";
            }
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        private string createDatabase(string path)
        {
            try
            {
                var connection = new SQLiteAsyncConnection(path);
                connection.CreateTableAsync<User>();
                connection.CreateTableAsync<Loans>();
                connection.CreateTableAsync<LoanApproval>();
                connection.CreateTableAsync<StaticDetails>();
                connection.CreateTableAsync<Bank>();
                connection.CreateTableAsync<Branch>();
                connection.CreateTableAsync<Sector>();
                connection.CreateTableAsync<clienttype>();
                connection.CreateTableAsync<Customer>();
                connection.CreateTableAsync<Purpose>();
                connection.CreateTableAsync<Products>();
                connection.CreateTableAsync<Amortization>();
                connection.CreateTableAsync<Repayments>();
               // connection.DropTableAsync<Amortization>();
               // connection.DropTableAsync<LoanApproval>();
                //  connection.DropTableAsync<Customer>();
                //  connection.DropTableAsync<StaticDetails>();
                return "Database created";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }


    }


        }
    



