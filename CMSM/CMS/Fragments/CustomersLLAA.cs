using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using CMS.Model;
using OldMutualAndroid.Model;
using System;
using System.Collections.Generic;
using static Android.Widget.TabHost;
using CMS.Activities;
using RestSharp;
using Newtonsoft.Json;
using System.Linq;
using SQLite;

namespace CMS.Fragments
{
    public class CustomersLLAA : Fragment
    {
        private List<string> mItems;
         private ListView mListView;
        public string path;
        CustomListQ  adapter;
        EditText bal;
        Button btnbal;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static CustomersLLAA NewInstance()
        {
            var frag1 = new CustomersLLAA { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.SearchMain, null);
            path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");

            mListView = view.FindViewById<ListView>(Resource.Id.listView);
           // bal= view.FindViewById<TextView>(Resource.Id.txt2);

           
            bal= view.FindViewById<EditText>(Resource.Id.netsalary);
          btnbal= view.FindViewById<Button>(Resource.Id.btnLogin);
            mListView.ItemLongClick += MListView_ItemLongClick;
            btnbal.Click += Btnbal_Click;
            return view;
        }

        private void Btnbal_Click(object sender, EventArgs e)
        {
            string search = bal.Text;
            if (search == "")
            {
                Toast.MakeText(this.Activity, "Search criteria required", ToastLength.Long).Show();

            }
            mItems = new List<string>();
            mItems = Customer(path,search);
            int[] imageId = new int[mItems.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in mItems)
            {

                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    imageId[x] = Resource.Drawable.ic_account_circle_black_24dp;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }


            //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());
            adapter = new CustomListQ(this.Activity, mItems, imageId);
            mListView.Adapter = adapter;
        }

        private void MListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            string data = adapter.GetItemAtPositionId(e.Position).ToString();

            //Toast.MakeText(this.Activity,, ToastLength.Long).Show();
            ApproverActivity.membernumber =data.Split(':')[1].ToString();
            ApproverActivity.loanno= data.Split(',')[0].ToString();
            ApproverActivity.loanno = ApproverActivity.loanno.ToString().Replace(" ","").Replace("LoanNumber","");
            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, LoanAppU.NewInstance(), "CustomersLLAA");
            trans2.AddToBackStack("CustomersLLAA");
            trans2.Commit();
        }

        private List<string> Customer(string path,string s)
        {
            List<string> my = new List<string>();
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                var query = db.Table<LoanApproval>().Where(a=>a.status== "LoanApplicationCapture");
                       
                               
                     foreach (var stock in query)
                {
                    if ((CustomerSSS(path,stock.membernumber) + ":" + stock.membernumber).ToLower().Replace(" ", "").ToString().Contains(s.ToLower().Replace(" ","").ToString()))
                    {
                      my.Add("Loan Number "+stock.loanNr +", Loan date,"+stock.appdate.ToString("dd-MMM-yyyy")+",\n"+ CustomerSSS(path, stock.membernumber) + ":" + stock.membernumber);
                    }
                }
                    

                // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return my;
        }
        private string CustomerSSS(string path, string s)
        {
            string n = "";
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                var query = db.Table<Customer>().Where(a=>a.CustomerNumber==s);
                          

                foreach (var p  in query)
                {
                    if (p.CustomerNumber==s)
                    {
                n = p.Fullnames.Split(',')[0];
                    }else
                    {
                        n = "NONAME";
                    }
                    
                }


                return n;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return n;
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}