using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace CMS.Fragments
{
    public class Fragment1 : Fragment
    {
        Button btn2, btn3,btn4,btn1,btn5,btn6;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Fragment1 NewInstance()
        {
            var frag1 = new Fragment1 { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.fragment1, null);
            btn1 = view.FindViewById<Button>(Resource.Id.button1);
            btn2 = view.FindViewById<Button>(Resource.Id.button2);
            btn3 = view.FindViewById<Button>(Resource.Id.button3);
            btn4 = view.FindViewById<Button>(Resource.Id.button4);
            btn5 = view.FindViewById<Button>(Resource.Id.button5);
            btn6 = view.FindViewById<Button>(Resource.Id.button6);
            btn1.Click += Btn1_Click;
            btn2.Click += Btn2_Click;
            btn3.Click += Btn3_Click;
            btn4.Click += Btn4_Click;
            btn5.Click += Btn5_Click;
            btn6.Click += Btn6_Click;
            return view;
        }

        private void Btn6_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity,"View Repayment Schedule", ToastLength.Long).Show();

        }

        private void Btn5_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "View Amortization", ToastLength.Long).Show();
        }

        private void Btn1_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loan Application", ToastLength.Long).Show();

        }

        private void Btn4_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "View Client Statement", ToastLength.Long).Show();

            //var trans2 = this.FragmentManager.BeginTransaction();
            //trans2.Replace(Resource.Id.content_frame, ChangePassword.NewInstance(), "ChangePassword");
            //trans2.AddToBackStack("Fragment1");
            //trans2.Commit();
        }

        private void Btn3_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Existing Client", ToastLength.Long).Show();

            //var trans2 = this.FragmentManager.BeginTransaction();
            //trans2.Replace(Resource.Id.content_frame, Loans.NewInstance(), "Loans");
            //trans2.AddToBackStack("Fragment1");
            //trans2.Commit();
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Create Account", ToastLength.Long).Show();

            //var trans2 = this.FragmentManager.BeginTransaction();
            //trans2.Replace(Resource.Id.content_frame, Borrower.NewInstance(), "Borrower");
            //trans2.AddToBackStack("Fragment1");
            //trans2.Commit();
        }


    }
}