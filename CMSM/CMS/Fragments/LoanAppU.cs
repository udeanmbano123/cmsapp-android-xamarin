﻿using Android;

using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using OldMutualAndroid.Model;
using System;
using System.Collections.Generic;
using TextDrawable;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;
using CMS.Model;
using Android.Support.V4.App;
using CMS.Activities;
using static Android.Widget.TabHost;
using SQLite;

namespace CMS.Fragments
{
    public class LoanAppU : Fragment
    {
        public EditText loannuus,member,Gname, GIdNo, Gphone, Grelationship, GAddress,loanamt,recamt,sourcrep;
        public TextView Dbirth,repadt,appdt,empdate;
        public EditText curremployer, empaddr, suprvme, empphone, empPosition, gross, netsal, phoneno;
        private List<string> product = new List<string>();
        Spinner products;
        private List<string> purpose = new List<string>();
        Spinner purposes;
        TextView _dateDisplay;
        Button _dateSelectButton;
        TextView _dateDisplay2;
        Button _dateSelectButton2;
        TextView _dateDisplay3;
        Button _dateSelectButton4;
        TextView _dateDisplay4;
        Button _dateSelectButton3;
        Button submit;
        RadioButton aa, aa2;
        EditText comm;
        public string loannu;
        // Create your fragment here
        public string path;
        public int samm;
        public static LoanAppU NewInstance()
        {
            var frag1 = new LoanAppU{ Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");

            View view = inflater.Inflate(Resource.Layout.ApplU, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Guarantor");
            spec1.SetIndicator("Guarantor Information");
            spec1.SetContent(Resource.Id.tab1);


            TabSpec spec2 = tabHost.NewTabSpec("Loan");
            spec2.SetIndicator("Loan Parameters");
            spec2.SetContent(Resource.Id.tab2);

            TabSpec spec3 = tabHost.NewTabSpec("Employment");
            spec3.SetIndicator("Employment Information");
            spec3.SetContent(Resource.Id.tab3);
;

            tabHost.AddTab(spec1);
            tabHost.AddTab(spec2);
            tabHost.AddTab(spec3);
            /* public EditText Gname, GIdNo, Gphone, Grelationship, GAddress, loanamt, recamt, sourcrep;
         public TextView Dbirth, repadt, appdt;
     */
            //Guarantor
            Gname=(EditText)view.FindViewById(Resource.Id.names);
            Gname.Enabled = false;
           GIdNo = (EditText)view.FindViewById(Resource.Id.idno);
            GIdNo.Enabled = false;
            Gphone = (EditText)view.FindViewById(Resource.Id.phone);
            Gphone.Enabled = false;
            Grelationship = (EditText)view.FindViewById(Resource.Id.rele);
            Grelationship.Enabled = false;
            GAddress = (EditText)view.FindViewById(Resource.Id.curr);
            GAddress.Enabled = false;
            loanamt = (EditText)view.FindViewById(Resource.Id.lamt);
            loanamt.Enabled = false;
            recamt = (EditText)view.FindViewById(Resource.Id.lramt);
            recamt.Enabled = false;
            loannuus = (EditText)view.FindViewById(Resource.Id.loannuu);
            loannuus.Enabled = false;
            sourcrep = (EditText)view.FindViewById(Resource.Id.srcamt);
            sourcrep.Enabled = false;
            //employee
            curremployer = (EditText)view.FindViewById(Resource.Id.curremployee);
            curremployer.Enabled = false;
            empaddr = (EditText)view.FindViewById(Resource.Id.empaddress);
            empaddr.Enabled = false;
            suprvme = (EditText)view.FindViewById(Resource.Id.supervisor);
            suprvme.Enabled = false;
            empphone = (EditText)view.FindViewById(Resource.Id.phone);
            empphone.Enabled = false;
            empPosition = (EditText)view.FindViewById(Resource.Id.empPosition);
            empPosition.Enabled = false;
            gross = (EditText)view.FindViewById(Resource.Id.grosssalary);
            gross.Enabled = false;
            netsal = (EditText)view.FindViewById(Resource.Id.netsalary);
            netsal.Enabled = false;
            Dbirth = (TextView)view.FindViewById(Resource.Id.dateDisplay);
            Dbirth.Enabled = false;
            repadt = (TextView)view.FindViewById(Resource.Id.dateDisplay2);
            repadt.Enabled = false;
            appdt = (TextView)view.FindViewById(Resource.Id.dateDisplay3);
            appdt.Enabled = false;
            empdate = (TextView)view.FindViewById(Resource.Id.dateDisplay4);
            empdate.Enabled = false;
            aa= (RadioButton)view.FindViewById(Resource.Id.aa1);
            aa2 = (RadioButton)view.FindViewById(Resource.Id.aa2);
            comm = (EditText)view.FindViewById(Resource.Id.comment);
            comm.Visibility = ViewStates.Invisible;
            _dateDisplay = view.FindViewById<TextView>(Resource.Id.aa2);
            _dateSelectButton = view.FindViewById<Button>(Resource.Id.dateSelectButton);
            _dateSelectButton.Enabled = false;
            _dateSelectButton.Click += _dateSelectButton_Click;
            _dateDisplay2 = view.FindViewById<TextView>(Resource.Id.dateDisplay2);
            _dateSelectButton2 = view.FindViewById<Button>(Resource.Id.dateSelectButton2);
            _dateSelectButton2.Enabled = false;
            _dateSelectButton2.Click += _dateSelectButton2_Click;
            _dateDisplay3 = view.FindViewById<TextView>(Resource.Id.dateDisplay3);
            _dateSelectButton3 = view.FindViewById<Button>(Resource.Id.dateSelectButton3);
            _dateSelectButton3.Enabled = false;
            _dateSelectButton3.Click += _dateSelectButton3_Click;
            _dateDisplay4 = view.FindViewById<TextView>(Resource.Id.dateDisplay4);
            _dateSelectButton4 = view.FindViewById<Button>(Resource.Id.dateSelectButton4);
            _dateSelectButton4.Click += _dateSelectButton4_Click;
            submit = (Button)view.FindViewById(Resource.Id.btnLogin);
              products=(Spinner)view.FindViewById(Resource.Id.product);
             purposes=(Spinner)view.FindViewById(Resource.Id.purposespin);
            member= (EditText)view.FindViewById(Resource.Id.member);
            member.Enabled = false;
            loannu = ApproverActivity.loanno;
            member.Text = ApproverActivity.membernumber;
            suprvme.Visibility = ViewStates.Invisible;
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<LoanApproval>().Where(v => v.loanNr == loannu);

            foreach (var stock in query)
            {
                samm = stock.ID;
              loannuus.Text = stock.loanNr;
               Gname.Text=stock.name;
               GIdNo.Text=stock.IDNo;
               Gphone.Text=stock.Phone;
               Grelationship.Text=stock.Relationship;
               GAddress.Text=stock.Address;
              Dbirth.Text=stock.dob.ToString();
                //loan
                product.Add(NumberALD(stock.product,path));
               loanamt.Text=stock.loanamounty.ToString();
               recamt.Text=stock.recamount.ToString();

                sourcrep.Text=stock.repaysource;
                purpose.Add(stock.loanpurpose);
               repadt.Text=stock.frepay.ToString();
                 appdt.Text=stock.appdate.ToString();

                //employment information
                 curremployer.Text=stock.currentemp;
                 empaddr.Text=stock.empaddress;


                empdate.Text = stock.dateemp.ToString();
                
                 empphone.Text=stock.PhoneNo;
               
                empPosition.Text=stock.Position;
               
                gross.Text=stock.grosssal.ToString();
           
                 netsal.Text=stock.netsalary.ToString();
                
                

            }
            submit.Click += Submit_Click;
            //product.Add("--Select Product--");
            //product.Add("Business 1"); 
            //product.Add("Good Clients");
            var adapter4 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, product);

            products.Adapter = adapter4;
            //purpose.Add("--Select Purpose--");
            //purpose.Add("Educational");
            //purpose.Add("Agricultural");
            
            var adapter5 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, purpose);

            purposes.Adapter = adapter5;
            aa2.Click += Aa2_Click;
            aa.Click += Aa_Click;
            return view;
        }

        private void Aa_Click(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            if (rb.Checked)
            {
                comm.Visibility = ViewStates.Invisible;
            }
           
        }

        private void Aa2_Click(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            if (rb.Checked)
            {
                comm.Visibility = ViewStates.Visible;
            }

        }
        public string NumberALD(string s, string path)
        {
            string n = "";
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<Products>().Where(v => v.Code == s);

            foreach (var p in query)
            {
                n = p.DisplayName;
            }

            return n;
        }
        private void _dateSelectButton4_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay4.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);

        }

        private void Submit_Click(object sender, EventArgs e)
        {
            //submit
            if (products.SelectedItem.ToString() == "--Select Product--")
            {

                Toast.MakeText(this.Activity, "Select product is required", ToastLength.Long).Show();

            }
            else if (loanamt.Text == "")
            {

                Toast.MakeText(this.Activity, "Loan amount is required", ToastLength.Long).Show();

            }
            else if (recamt.Text == "")
            {

                Toast.MakeText(this.Activity, "Recommended amount is required", ToastLength.Long).Show();

            }
            else if (_dateDisplay2.Text == "")
            {

                Toast.MakeText(this.Activity, "Application date is required", ToastLength.Long).Show();

            }
            else if (_dateDisplay3.Text == "")
            {

                Toast.MakeText(this.Activity, "Repayment date is required", ToastLength.Long).Show();

            } else if (aa.Checked==false && aa2.Checked==false) {

                Toast.MakeText(this.Activity, "The loan must be approved or rejected", ToastLength.Long).Show();

            }
            else
            {
                Model.LoanApproval my = new Model.LoanApproval();
                my.ID = samm;
                my.membernumber = member.Text;
                //guarantor
                my.name = Gname.Text;
                if (my.name == "")
                {
                    my.name = "NONE";
                }
                my.IDNo = GIdNo.Text;
                if (my.IDNo == "")
                {
                    my.IDNo = "NONE";
                }
                my.PhoneNo = Gphone.Text;
                if (my.PhoneNo == "")
                {
                    my.PhoneNo = "NONE";
                }
                my.Relationship = Grelationship.Text;
                if (my.Relationship == "")
                {
                    my.Relationship = "NONE";
                }
                my.Address = GAddress.Text;
                if (my.Address == "")
                {
                    my.Address = "NONE";
                }
                try
                {
                    my.dob = Convert.ToDateTime(Dbirth.Text);
                }
                catch (Exception)
                {

                    my.dob = Convert.ToDateTime(null);
                }

                //loan
                my.product = products.SelectedItem.ToString();
                if (my.product == "")
                {
                    my.product = "NONE";
                }
                my.loanamounty = Convert.ToDecimal(loanamt.Text);

                my.recamount = Convert.ToDecimal(recamt.Text);

                my.repaysource = sourcrep.Text;
                if (my.repaysource == "")
                {
                    my.repaysource = "NONE";
                }

                my.loanpurpose = purposes.SelectedItem.ToString();
                if (my.loanpurpose == "")
                {
                    my.loanpurpose = "NONE";
                }

                my.frepay = Convert.ToDateTime(repadt.Text);

                my.appdate = Convert.ToDateTime(appdt.Text);

                //employment information
                my.currentemp = curremployer.Text;
                if (my.currentemp == "")
                {
                    my.currentemp = "NONE";
                }
                my.empaddress = empaddr.Text;
                if (my.empaddress == "")
                {
                    my.empaddress = "NONE";
                }
                try
                {
                    my.dateemp = Convert.ToDateTime(empdate.Text);
                }
                catch (Exception)
                {

                    my.dateemp = DateTime.Now;
                }
                my.Supervisor = suprvme.Text;
                if (my.Supervisor == "")
                {
                    my.Supervisor = "NONE";
                }
                my.Phone = empphone.Text;
                if (my.Phone == "")
                {
                    my.Phone = "NONE";
                }
                my.Position = empPosition.Text;
                if (my.Position == "")
                {
                    my.Position = "NONE";
                }
                try
                {
                    my.grosssal = Convert.ToDecimal(gross.Text);
                }
                catch (Exception)
                {

                    my.grosssal = 0;
                }
                try
                {
                    my.netsalary = Convert.ToDecimal(netsal.Text);

                }
                catch (Exception)
                {

                    my.netsalary = 0;
                }
                my.username = MainActivity.username;
                if (aa.Checked)
                {
                my.refrr = "SUBMITTED";
                my.status = "SUBMITTED";
                }else if (aa2.Checked)
                {
                    my.refrr = "REJECTED";
                    my.status = "REJECTED";
                }
            
                my.dateCreated = DateTime.Now.ToString();
                my.loanNr = ApproverActivity.loanno;
                my.reason = comm.Text;
                if (my.reason == "")
                {
                    my.reason = "NONE";
                }

                string app = insertUpdateData(my, path);

                if (app == "Updated")
                {
                    Toast.MakeText(this.Activity, "Loan approval process was successful", ToastLength.Long).Show();
                    var trans2 = this.FragmentManager.BeginTransaction();
                    trans2.Replace(Resource.Id.content_frame, CustomersLLAA.NewInstance(), "LoanAppU");
                    trans2.AddToBackStack("LoanAppU");
                    trans2.Commit();
                }
                else
                {
                    Toast.MakeText(this.Activity, "Loan approval process was successful", ToastLength.Long).Show();

                }
            }
           

        }

        private void _dateSelectButton3_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay3.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private void _dateSelectButton2_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay2.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private void _dateSelectButton_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }
        private string insertUpdateData(Model.LoanApproval data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.UpdateAsync(data);
                return "Updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
    }
}
 