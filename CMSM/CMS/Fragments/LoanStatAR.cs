using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using CMS.Model;
using OldMutualAndroid.Model;
using System;
using System.Collections.Generic;
using static Android.Widget.TabHost;
using CMS.Activities;
using RestSharp;
using Newtonsoft.Json;
using System.Linq;
using SQLite;

namespace CMS.Fragments
{
    public class LoanStatAR : Fragment
    {
        private List<Repayments> mItems;
         private ListView mListView;
        private ListView mListView2;
        CustomListSMR  adapter;
        TextView bal;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static LoanStatAR NewInstance()
        {
            var frag1 = new LoanStatAR { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.LoanStatementAR, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Loan Repayment Summary");
            spec1.SetContent(Resource.Id.tab1);
            spec1.SetIndicator("Loan Repayment Summary");

           tabHost.AddTab(spec1);
            //loanbalance

            mListView = view.FindViewById<ListView>(Resource.Id.listView1);
            bal= view.FindViewById<TextView>(Resource.Id.txt2);
            string path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");

            bal.Text = "";
            mItems = new List<Repayments>();
            try
            {
          
               var dbsel = NumberAm(MainActivity.loanno,path);
                int me = dbsel.Count();
                int count = 0;
                DateTime new1=new DateTime();
                foreach (var p in dbsel)
                {
                    
                  
                        mItems.Add(new Repayments() { custno = NumberAmm(p.custno,path), loanID = p.loanID, loanamount = p.loanamount.ToString(), paymentdate = p.paymentdate.ToString(), monthly = p.monthly.ToString() });
                    
                    }



                Drawable[] imageId = new Drawable[mItems.Count];
                int x = 0;
                TextDrawable.TextDrawable drawable;
                foreach (var d in mItems)
                {

                    try
                    {
                        //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                         drawable = TextDrawable.TextDrawable.TextDrawbleBuilder.BeginConfig().FontSize(34).TextColor(Color.Black).Bold().EndConfig().BuildRound(mItems.Count().ToString(), GetRandomColor());
                        imageId[x] = drawable;
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    x += 1;
                }


                //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());
                adapter = new CustomListSMR(this.Activity, mItems, imageId);
                mListView.Adapter = adapter;

            }
            catch (Exception)
            {

              
            } return view;
        }
        public List<Repayment> NumberAm(string s, string path)
        {
            int ss = 0;
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<Repayment>().Where(v => v.loanID == s);


            return query.ToList();
        }
        public string NumberAmm(string s, string path)
        {
            string ss = "";
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<Customer>().Where(v => v.CustomerNumber == s);

            foreach (var d in query)
            {
                return d.Fullnames;
            }

            return ss;
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}