﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CMS.Model;
using SQLite;
using RestSharp;
using CMS.Activities;
using Newtonsoft.Json;
using System.IO;

namespace CMS.Intents
{
    [Service]
    public class OfficerIntent : IntentService
    {

        public string path;
        const int NOTIFICATION_ID = 9009;
        public OfficerIntent() : base("OfficerIntent")
        {
        }

        protected override void OnHandleIntent(Android.Content.Intent intent)
        {
            //path
             path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");
             //sync Officer data
           // Console.WriteLine("Credit Management data update");
            Notification.Builder notificationBuilder = new Notification.Builder(this)
       .SetSmallIcon(Resource.Drawable.qqsmall)
       .SetContentTitle("DATA UPDATES")
       .SetContentText("Loan officer data update.");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

            try
            {  clienttypes();
                Purposes();
             Customers();
                sector();
                Banks();
                              
                Products();
                  Branches();
                StaticGet(path);
                LoansS(path);
                Amortization(path);
                Repayment(path);
           
          

            }
            catch (Exception)
            {

           
            }
        //Console.WriteLine("work complete");
        }

        public void Amortization(string path)
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("amortization/{user}", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request.AddUrlSegment("user", MainActivity.username.ToString().Replace(" ", "").Replace("*", "-"));

            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Amortizations> dataList = JsonConvert.DeserializeObject<List<Amortizations>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                try
                {
                    Amortization nn = new Amortization();
                    nn.loanID = p.loanID;
                    try 
	{	        
		nn.loanamount = Convert.ToDecimal(p.loanamount);
	}
	catch (Exception e)
	{

                        nn.loanamount = 0;
	}
                    try
                    {
                        nn.monthly = Convert.ToDecimal(p.monthly);
                    }
                    catch (Exception e)
                    {

                        nn.monthly = 0;
                    }
                    nn.paymentdate = Convert.ToDateTime(p.paymentdate);
                    nn.refs = p.refs;
                    nn.custno = p.custno;
                    int my = NumberAm(nn.refs, path);
                    if (my <= 0)
                    {
                        insertUpdateDataAm(nn, path);
                    }
                }
                catch (Exception)
                {

                    continue;
                }
               
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
 .SetSmallIcon(Resource.Drawable.qqsmall)
 .SetContentTitle("DATA UPDATES")
 .SetContentText("Amortization have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);

        }
        private string insertUpdateDataAm(Amortization data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }



        public int NumberAm(string s, string path)
        {
            int ss = 0;
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<Amortization>().Where(v => v.refs == s);

            
            return query.Count();
        }
        //Repayments

        public void Repayment(string path)
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("repayment/{user}", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request.AddUrlSegment("user", MainActivity.username.ToString().Replace(" ", "").Replace("*", "-"));

            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Repayments> dataList = JsonConvert.DeserializeObject<List<Repayments>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                try
                {
                    Repayment nn = new Repayment();
                    nn.loanID = p.loanID;
                    try
                    {
                        nn.loanamount = Convert.ToDecimal(p.loanamount);
                    }
                    catch (Exception e)
                    {

                        nn.loanamount = 0;
                    }
                    try
                    {
                        nn.monthly = Convert.ToDecimal(p.monthly);
                    }
                    catch (Exception e)
                    {

                        nn.monthly = 0;
                    }
                    nn.paymentdate = Convert.ToDateTime(p.paymentdate);
                    nn.refs = p.refs;
                    nn.custno = p.custno;
                    int my = NumberAmR(nn.refs, path);
                    if (my <= 0)
                    {
                        insertUpdateDataAmR(nn, path);
                    }
                }
                catch (Exception)
                {

                    continue;
                }

            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
 .SetSmallIcon(Resource.Drawable.qqsmall)
 .SetContentTitle("DATA UPDATES")
 .SetContentText("Repayment have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);

        }
        private string insertUpdateDataAmR(Repayment data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }



        public int NumberAmR(string s, string path)
        {
            int ss = 0;
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<Repayment>().Where(v => v.refs == s);


            return query.Count();
        }
        public void LoansS(string path)
        {
            // send captured loans to approve
            var db = new SQLiteConnection(path);
            var query = db.Table<LoanApproval>().Where(v => v.refrr == "NEW");
            foreach (var stock in query)
            {
                try 
	{	        
		var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("LoanApplication/{customernumber}/{name}/{id}/{phone}/{rel}/{curraddr}/{dob}/{product}/{loanamount}/{recamount}/{sourcerep}/{purpose}/{repaydte}/{appldate}/{user}", Method.POST);
                request.AddUrlSegment("customernumber", stock.membernumber.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("name", stock.name.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("id", stock.IDNo.Replace("/", "-"));
                request.AddUrlSegment("phone", stock.Phone.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("rel", stock.Relationship.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("curraddr", stock.Address.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("dob", stock.dob.ToString("dd-MMM-yyyy"));
                request.AddUrlSegment("product", stock.product.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("loanamount", stock.loanamounty.ToString().Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("recamount", stock.recamount.ToString().Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("sourcerep", stock.repaysource.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("purpose", stock.loanpurpose.Replace(" ", "").Replace("*", "-"));
                request.AddUrlSegment("repaydte", stock.frepay.ToString("dd-MMM-yyyy"));
                request.AddUrlSegment("appldate", stock.appdate.ToString("dd-MMM-yyyy"));
                request.AddUrlSegment("user", stock.username.Replace(" ", "").Replace("*", "-"));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                var Jsonobject = JsonConvert.DeserializeObject<string>(validate);
                validate = Jsonobject.ToString();


                stock.membernumber = stock.membernumber;
                //guarantor
                stock.name = stock.name;

                stock.IDNo = stock.IDNo;

                stock.PhoneNo = stock.PhoneNo;

                stock.Relationship = stock.Relationship;

                stock.Address = stock.Address;


                stock.dob = stock.dob;

                //loan
                stock.product = stock.product;
                stock.loanamounty = stock.loanamounty;

                stock.recamount = stock.recamount;

                stock.repaysource = stock.repaysource;

                stock.loanpurpose = stock.loanpurpose;

                stock.frepay = stock.frepay;

                stock.appdate = stock.appdate;

                //employment information
                stock.currentemp = stock.currentemp;
                stock.empaddress = stock.empaddress;
                stock.dateemp = stock.dateemp;
                stock.Supervisor = stock.Supervisor;

                stock.Phone = stock.Phone;

                stock.Position = stock.Position;

                stock.grosssal = stock.grosssal;

                stock.netsalary = stock.netsalary;

                stock.username = stock.username;

                stock.refrr = "APPROVE";

                stock.dateCreated = stock.dateCreated;

                stock.status = "LoanApplicationCapture";
                stock.loanstatus = "LoanApplicationCapture";

                stock.loanNr = validate;
                stock.ID = stock.ID;
                insertUpdateDataLoans(stock, path);

	}
	catch (Exception e)
	{

                    continue;
	}
    }

        }
        public void StaticGet(string path)
        {

            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            try
            {
                var query = db.Table<StaticDetails>().Where(v => v.CustomerNumber == "PENDING");

                foreach (var stock in query)
                {
                    try
                    {


                        stock.Id = stock.Id;
                        var client = new RestClient(MainActivity.baseUrl);
                        var request = new RestRequest("Stat/{clienttype}/{applicatiotype}/{surname}/{forenames}/{id}/{sector}/{dob}/{address}/{city}/{area}/{gender}/{marital}/{phoneno}/{spname}/{spphone}/{occup}/{numdep}/{bank}/{branch}/{account}/{branchcode}/{curremployer}/{empaddr}/{empdate}/{superv}/{phone}/{position}/{gross}/{netsal}/{user}", Method.POST);
                        request.AddUrlSegment("clienttype", stock.clienttype);
                        request.AddUrlSegment("applicatiotype", stock.applicationtype.ToString().Replace(" ", ""));
                        request.AddUrlSegment("surname", stock.Surname.ToString().Replace(" ", ""));
                        request.AddUrlSegment("forenames", stock.Forenames.ToString().Replace(" ", ""));
                        request.AddUrlSegment("id", stock.NRC.ToString().Replace(" ", "").Replace("/", "-"));
                        request.AddUrlSegment("sector", stock.sector.ToString().Replace(" ", ""));
                        request.AddUrlSegment("dob", stock.dsteofbirth.ToString("dd-MMM-yyyy").ToString().Replace(" ", ""));
                        request.AddUrlSegment("address", stock.Address.ToString().Replace(" ", ""));
                        request.AddUrlSegment("city", stock.City.ToString().Replace(" ", ""));
                        request.AddUrlSegment("area", stock.area.ToString().Replace(" ", ""));
                        request.AddUrlSegment("gender", stock.gender.ToString().Replace(" ", ""));
                        request.AddUrlSegment("marital", stock.martialstatus.ToString().Replace(" ", ""));
                        request.AddUrlSegment("phoneno", stock.phonenumber.ToString().Replace(" ", ""));
                        request.AddUrlSegment("spname", stock.spousename.ToString().Replace(" ", ""));
                        request.AddUrlSegment("spphone", stock.spousephone.ToString().Replace(" ", ""));
                        request.AddUrlSegment("occup", stock.Occupation.ToString().Replace(" ", ""));
                        request.AddUrlSegment("numdep", stock.numberofdependants.ToString());
                        request.AddUrlSegment("bank", stock.Bank.ToString().Replace(" ", ""));
                        request.AddUrlSegment("branch", stock.Branch.ToString().Replace(" ", ""));
                        request.AddUrlSegment("account", stock.AccountNumber.ToString().Replace(" ", ""));
                        request.AddUrlSegment("branchcode", stock.branchcode.ToString().Replace(" ", ""));
                        request.AddUrlSegment("curremployer", stock.currentemp.ToString().Replace(" ", ""));
                        request.AddUrlSegment("empaddr", stock.empaddress.ToString().Replace(" ", ""));
                        request.AddUrlSegment("empdate", Convert.ToDateTime(stock.dateemp).ToString("dd-MMM-yyyy"));
                        request.AddUrlSegment("superv", stock.Supervisor.ToString().Replace(" ", ""));
                        request.AddUrlSegment("phone", stock.Phone.ToString().Replace(" ", ""));
                        request.AddUrlSegment("position", stock.Position.ToString().Replace(" ", ""));
                        request.AddUrlSegment("gross", stock.grosssal.ToString().Replace(" ", ""));
                        request.AddUrlSegment("netsal", stock.netsalary.ToString());
                        request.AddUrlSegment("user", MainActivity.username.ToString().Replace(" ", ""));
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;

                        var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

                        validate = Jsonobject.ToString();
                        stock.CustomerNumber = validate;
                        StaticDetails myy = new StaticDetails();
                        myy.Id = stock.Id;
                        myy.CustomerNumber = stock.CustomerNumber;
                        myy.clienttype = stock.clienttype;
                        myy.applicationtype = stock.applicationtype;
                        myy.Surname = stock.Surname;
                        myy.Forenames = stock.Forenames;
                        myy.NRC = stock.NRC;
                        myy.sector = stock.sector;
                        myy.dsteofbirth = stock.dsteofbirth;
                        myy.Address = stock.Address;
                        myy.City = stock.City;
                        myy.area = stock.area;
                        myy.gender = stock.gender;
                        myy.martialstatus = stock.martialstatus;
                        myy.phonenumber = stock.phonenumber;
                        myy.spousename = stock.spousename;
                        myy.spousephone = stock.spousephone;
                        myy.Occupation = stock.Occupation;
                        myy.numberofdependants = stock.numberofdependants;
                        //Bsanking
                        myy.Bank = stock.Bank;
                        myy.Branch = stock.Branch;

                        myy.AccountNumber = stock.AccountNumber;

                        myy.branchcode = stock.branchcode;

                        //employer
                        myy.currentemp = stock.currentemp;
                        myy.empaddress = stock.empaddress;

                        myy.dateemp = stock.dateemp;

                        myy.Supervisor = stock.Supervisor;

                        myy.Phone = stock.Phone;

                        myy.Position = stock.Position;

                        myy.grosssal = stock.grosssal;

                        myy.netsalary = stock.netsalary;


                        myy.username = stock.username;

                        myy.refrr = stock.refrr;

                        myy.dateCreated = stock.dateCreated;
                        myy.action = stock.action;
                        if (myy.CustomerNumber == "Loan Officer not registered")
                        {
                            myy.CustomerNumber = "PENDING";
                        }
                        insertUpdateDataStatic(myy, path);
                    }
                    catch (Exception)
                    {

                        continue;
                    }
                }
            }
            catch (Exception)
            {

                
            }

            //Update
            try
            {
                var query2 = db.Table<StaticDetails>().Where(v => v.refrr == "UPDATE" && v.CustomerNumber != "PENDING");

                foreach (var stock in query2)
                {
                    try
                    {

                        stock.Id = stock.Id;
                        var client = new RestClient(MainActivity.baseUrl);
                        var request = new RestRequest("StatU/{customernumber}/{clienttype}/{applicatiotype}/{surname}/{forenames}/{id}/{sector}/{dob}/{address}/{city}/{area}/{gender}/{marital}/{phoneno}/{spname}/{spphone}/{occup}/{numdep}/{bank}/{branch}/{account}/{branchcode}/{curremployer}/{empaddr}/{empdate}/{superv}/{phone}/{position}/{gross}/{netsal}/{user}", Method.PUT);
                        request.AddUrlSegment("customernumber", stock.CustomerNumber.ToString().Replace(" ", "").Replace("*", "-"));
                        request.AddUrlSegment("clienttype", stock.clienttype);
                        request.AddUrlSegment("applicatiotype", stock.applicationtype.ToString().Replace(" ", ""));
                        request.AddUrlSegment("surname", stock.Surname.ToString().Replace(" ", ""));
                        request.AddUrlSegment("forenames", stock.Forenames.ToString().Replace(" ", ""));
                        request.AddUrlSegment("id", stock.NRC.ToString().Replace(" ", "").Replace("/", "-"));
                        request.AddUrlSegment("sector", stock.sector.ToString().Replace(" ", ""));
                        request.AddUrlSegment("dob", stock.dsteofbirth.ToString("dd-MMM-yyyy").ToString().Replace(" ", ""));
                        request.AddUrlSegment("address", stock.Address.ToString().Replace(" ", ""));
                        request.AddUrlSegment("city", stock.City.ToString().Replace(" ", ""));
                        request.AddUrlSegment("area", stock.area.ToString().Replace(" ", ""));
                        request.AddUrlSegment("gender", stock.gender.ToString().Replace(" ", ""));
                        request.AddUrlSegment("marital", stock.martialstatus.ToString().Replace(" ", ""));
                        request.AddUrlSegment("phoneno", stock.phonenumber.ToString().Replace(" ", ""));
                        request.AddUrlSegment("spname", stock.spousename.ToString().Replace(" ", ""));
                        request.AddUrlSegment("spphone", stock.spousephone.ToString().Replace(" ", ""));
                        request.AddUrlSegment("occup", stock.Occupation.ToString().Replace(" ", ""));
                        request.AddUrlSegment("numdep", stock.numberofdependants.ToString());
                        request.AddUrlSegment("bank", stock.Bank.ToString().Replace(" ", ""));
                        request.AddUrlSegment("branch", stock.Branch.ToString().Replace(" ", ""));
                        request.AddUrlSegment("account", stock.AccountNumber.ToString().Replace(" ", ""));
                        request.AddUrlSegment("branchcode", stock.branchcode.ToString().Replace(" ", ""));
                        request.AddUrlSegment("curremployer", stock.currentemp.ToString().Replace(" ", ""));
                        request.AddUrlSegment("empaddr", stock.empaddress.ToString().Replace(" ", ""));
                        request.AddUrlSegment("empdate", Convert.ToDateTime(stock.dateemp).ToString("dd-MMM-yyyy"));
                        request.AddUrlSegment("superv", stock.Supervisor.ToString().Replace(" ", ""));
                        request.AddUrlSegment("phone", stock.Phone.ToString().Replace(" ", ""));
                        request.AddUrlSegment("position", stock.Position.ToString().Replace(" ", ""));
                        request.AddUrlSegment("gross", stock.grosssal.ToString().Replace(" ", ""));
                        request.AddUrlSegment("netsal", stock.netsalary.ToString());
                        request.AddUrlSegment("user", MainActivity.username.ToString().Replace(" ", ""));
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;

                        var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

                        validate = Jsonobject.ToString();
                        stock.refrr = validate;
                        StaticDetails myy = new StaticDetails();
                        myy.Id = stock.Id;
                        myy.CustomerNumber = stock.CustomerNumber;
                        myy.clienttype = stock.clienttype;
                        myy.applicationtype = stock.applicationtype;
                        myy.Surname = stock.Surname;
                        myy.Forenames = stock.Forenames;
                        myy.NRC = stock.NRC;
                        myy.sector = stock.sector;
                        myy.dsteofbirth = stock.dsteofbirth;
                        myy.Address = stock.Address;
                        myy.City = stock.City;
                        myy.area = stock.area;
                        myy.gender = stock.gender;
                        myy.martialstatus = stock.martialstatus;
                        myy.phonenumber = stock.phonenumber;
                        myy.spousename = stock.spousename;
                        myy.spousephone = stock.spousephone;
                        myy.Occupation = stock.Occupation;
                        myy.numberofdependants = stock.numberofdependants;
                        //Bsanking
                        myy.Bank = stock.Bank;
                        myy.Branch = stock.Branch;

                        myy.AccountNumber = stock.AccountNumber;

                        myy.branchcode = stock.branchcode;

                        //employer
                        myy.currentemp = stock.currentemp;
                        myy.empaddress = stock.empaddress;

                        myy.dateemp = stock.dateemp;

                        myy.Supervisor = stock.Supervisor;

                        myy.Phone = stock.Phone;

                        myy.Position = stock.Position;

                        myy.grosssal = stock.grosssal;

                        myy.netsalary = stock.netsalary;


                        myy.username = stock.username;

                        myy.refrr = stock.refrr;

                        myy.dateCreated = stock.dateCreated;
                        myy.action = stock.action;
                        if (myy.CustomerNumber == "SENT")
                        {
                            myy.refrr = "PROCESSED";
                            myy.action = "PROCESSED";
                        }
                        insertUpdateDataStatic(myy, path);
                    }
                    catch (Exception)
                    {

                        continue;
                    }
                }
            }
            catch (Exception)
            {

                
            }

            //updates for static
            try
            {

                var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("StatUU/{user}", Method.GET);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                        request.AddUrlSegment("user", MainActivity.username.Replace(" ", ""));
                IRestResponse response = client.Execute(request);
       
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<StaticDetailsUpload> dataList = JsonConvert.DeserializeObject<List<StaticDetailsUpload>>(validate);

                var z = from s in dataList
                        select s;
                int myv = z.Count();
                foreach (var f in z)
                {
                    StaticDetails my = new StaticDetails();
                    my.CustomerNumber = f.customernumber;
                    my.clienttype = f.clienttype;
                    my.applicationtype = f.applicatiotype;
                    my.Surname = f.surname;
                    my.Forenames = f.forenames;
                    my.NRC = f.id;
                    my.sector = f.sector;
                    try
                    {
                        my.dsteofbirth = Convert.ToDateTime(f.dob);
                    }
                    catch (Exception)
                    {

                        my.dsteofbirth = DateTime.Now;
                    }
                    my.Address = f.address;
                    my.City = f.City;
                    my.area = f.area;
                    my.gender = f.gender;
                    my.martialstatus = f.martial;
                    my.phonenumber = f.phoneno;
                    //spouse
                    my.spousename = f.spname;
                    my.spousephone = f.spphone;
                    my.Occupation = f.occup;
                    try
                    {
                        my.numberofdependants = Convert.ToInt32(f.numdep);
                    }
                    catch (Exception)
                    {

                        my.numberofdependants = 0;
                    }
                    my.Bank = f.bank;
                    my.Branch = f.branch;
                    my.AccountNumber = f.account;
                    my.branchcode = f.branchcode;
                    my.currentemp = f.currentemp;
                    my.empaddress = f.empaddr;
                    my.dateemp = f.empdate;
                    my.Supervisor = f.superv;
                    my.Phone = f.Phone;
                    my.Position = f.Position;
                    try
                    {
                        my.grosssal = Convert.ToDecimal(f.gross);
                    }
                    catch (Exception)
                    {

                        my.grosssal = 0;
                    }
                    try
                    {
                        my.netsalary = Convert.ToDecimal(f.netsal);
                    }
                    catch (Exception)
                    {

                        my.netsalary = 0;
                    }
                    my.username = MainActivity.username;
                    my.refrr = "PROCESSED";
                    my.action = "PROCESSED";
                    if (Number(my.CustomerNumber,path)==0)
                    {

                        insertUpdateDataStaticA(my, path);
                    }
                    else if (Number(my.CustomerNumber, path) >= 0)
                    {
                        my.Id = NumberA(my.CustomerNumber, path);
                        insertUpdateDataStatic(my, path);
                    }
                }

            }
            catch (Exception)
            {

              
            }

            //Approvals
            //updates for static
            try
            {

                var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("StatUUA/{user}", Method.GET);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                request.AddUrlSegment("user", MainActivity.username.Replace(" ", ""));
                IRestResponse response = client.Execute(request);

                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<StaticDetailsUpload> dataList = JsonConvert.DeserializeObject<List<StaticDetailsUpload>>(validate);

                var z = from s in dataList
                        select s;
                int myv = z.Count();
                foreach (var f in z)
                {
                    StaticDetails my = new StaticDetails();
                    my.CustomerNumber = f.customernumber;
                    my.clienttype = f.clienttype;
                    my.applicationtype = f.applicatiotype;
                    my.Surname = f.surname;
                    my.Forenames = f.forenames;
                    my.NRC = f.id;
                    my.sector = f.sector;
                    try
                    {
                        my.dsteofbirth = Convert.ToDateTime(f.dob);
                    }
                    catch (Exception)
                    {

                        my.dsteofbirth = DateTime.Now;
                    }
                    my.Address = f.address;
                    my.City = f.City;
                    my.area = f.area;
                    my.gender = f.gender;
                    my.martialstatus = f.martial;
                    my.phonenumber = f.phoneno;
                    //spouse
                    my.spousename = f.spname;
                    my.spousephone = f.spphone;
                    my.Occupation = f.occup;
                    try
                    {
                        my.numberofdependants = Convert.ToInt32(f.numdep);
                    }
                    catch (Exception)
                    {

                        my.numberofdependants = 0;
                    }
                    my.Bank = f.bank;
                    my.Branch = f.branch;
                    my.AccountNumber = f.account;
                    my.branchcode = f.branchcode;
                    my.currentemp = f.currentemp;
                    my.empaddress = f.empaddr;
                    my.dateemp = f.empdate;
                    my.Supervisor = f.superv;
                    my.Phone = f.Phone;
                    my.Position = f.Position;
                    try
                    {
                        my.grosssal = Convert.ToDecimal(f.gross);
                    }
                    catch (Exception)
                    {

                        my.grosssal = 0;
                    }
                    try
                    {
                        my.netsalary = Convert.ToDecimal(f.netsal);
                    }
                    catch (Exception)
                    {

                        my.netsalary = 0;
                    }
                    my.username = MainActivity.username;
               
                    my.Id = NumberA(my.CustomerNumber, path);
                  
                    if (my.Id != 0 && my.refrr=="TOAPPROVE")
                    {
                        my.refrr = "APPROVE";
                    my.action = "APPROVE";

                        insertUpdateDataStatic(my, path);
                    }
                }

            }
            catch (Exception)
            {


            }
            //ToApprove
            try
            {
                var query2 = db.Table<StaticDetails>().Where(v => v.refrr == "TOAPPROVE" && v.CustomerNumber != "PENDING");

                foreach (var stock in query2)
                {
                    try
                    {
                        string username = "";

                        if (username == "")
                        {
                            username = MainActivity.username.ToString();
                        }

                        stock.Id = stock.Id;
                        var client = new RestClient(MainActivity.baseUrl);
                        var request = new RestRequest("StatUC/{customernumber}/{clienttype}/{applicatiotype}/{surname}/{forenames}/{id}/{sector}/{dob}/{address}/{city}/{area}/{gender}/{marital}/{phoneno}/{spname}/{spphone}/{occup}/{numdep}/{bank}/{branch}/{account}/{branchcode}/{curremployer}/{empaddr}/{empdate}/{superv}/{phone}/{position}/{gross}/{netsal}/{user}", Method.PUT);
                        request.AddUrlSegment("customernumber", stock.CustomerNumber.ToString().Replace(" ", "").Replace("*", "-"));
                        request.AddUrlSegment("clienttype", stock.clienttype);
                        request.AddUrlSegment("applicatiotype", stock.applicationtype.ToString().Replace(" ", ""));
                        request.AddUrlSegment("surname", stock.Surname.ToString().Replace(" ", ""));
                        request.AddUrlSegment("forenames", stock.Forenames.ToString().Replace(" ", ""));
                        request.AddUrlSegment("id", stock.NRC.ToString().Replace(" ", "").Replace("/", "-"));
                        request.AddUrlSegment("sector", stock.sector.ToString().Replace(" ", ""));
                        request.AddUrlSegment("dob", stock.dsteofbirth.ToString("dd-MMM-yyyy").ToString().Replace(" ", ""));
                        request.AddUrlSegment("address", stock.Address.ToString().Replace(" ", ""));
                        request.AddUrlSegment("city", stock.City.ToString().Replace(" ", ""));
                        request.AddUrlSegment("area", stock.area.ToString().Replace(" ", ""));
                        request.AddUrlSegment("gender", stock.gender.ToString().Replace(" ", ""));
                        request.AddUrlSegment("marital", stock.martialstatus.ToString().Replace(" ", ""));
                        request.AddUrlSegment("phoneno", stock.phonenumber.ToString().Replace(" ", ""));
                        request.AddUrlSegment("spname", stock.spousename.ToString().Replace(" ", ""));
                        request.AddUrlSegment("spphone", stock.spousephone.ToString().Replace(" ", ""));
                        request.AddUrlSegment("occup", stock.Occupation.ToString().Replace(" ", ""));
                        request.AddUrlSegment("numdep", stock.numberofdependants.ToString());
                        request.AddUrlSegment("bank", stock.Bank.ToString().Replace(" ", ""));
                        request.AddUrlSegment("branch", stock.Branch.ToString().Replace(" ", ""));
                        request.AddUrlSegment("account", stock.AccountNumber.ToString().Replace(" ", ""));
                        request.AddUrlSegment("branchcode", stock.branchcode.ToString().Replace(" ", ""));
                        request.AddUrlSegment("curremployer", stock.currentemp.ToString().Replace(" ", ""));
                        request.AddUrlSegment("empaddr", stock.empaddress.ToString().Replace(" ", ""));
                        request.AddUrlSegment("empdate", Convert.ToDateTime(stock.dateemp).ToString("dd-MMM-yyyy"));
                        request.AddUrlSegment("superv", stock.Supervisor.ToString().Replace(" ", ""));
                        request.AddUrlSegment("phone", stock.Phone.ToString().Replace(" ", ""));
                        request.AddUrlSegment("position", stock.Position.ToString().Replace(" ", ""));
                        request.AddUrlSegment("gross", stock.grosssal.ToString().Replace(" ", ""));
                        request.AddUrlSegment("netsal", stock.netsalary.ToString());
                        request.AddUrlSegment("user", username.ToString().Replace(" ", ""));
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;

                        var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

                        validate = Jsonobject.ToString();
                        stock.refrr = validate;
                        StaticDetails myy = new StaticDetails();
                        myy.Id = stock.Id;
                        myy.CustomerNumber = stock.CustomerNumber;
                        myy.clienttype = stock.clienttype;
                        myy.applicationtype = stock.applicationtype;
                        myy.Surname = stock.Surname;
                        myy.Forenames = stock.Forenames;
                        myy.NRC = stock.NRC;
                        myy.sector = stock.sector;
                        myy.dsteofbirth = stock.dsteofbirth;
                        myy.Address = stock.Address;
                        myy.City = stock.City;
                        myy.area = stock.area;
                        myy.gender = stock.gender;
                        myy.martialstatus = stock.martialstatus;
                        myy.phonenumber = stock.phonenumber;
                        myy.spousename = stock.spousename;
                        myy.spousephone = stock.spousephone;
                        myy.Occupation = stock.Occupation;
                        myy.numberofdependants = stock.numberofdependants;
                        //Bsanking
                        myy.Bank = stock.Bank;
                        myy.Branch = stock.Branch;

                        myy.AccountNumber = stock.AccountNumber;

                        myy.branchcode = stock.branchcode;

                        //employer
                        myy.currentemp = stock.currentemp;
                        myy.empaddress = stock.empaddress;

                        myy.dateemp = stock.dateemp;

                        myy.Supervisor = stock.Supervisor;

                        myy.Phone = stock.Phone;

                        myy.Position = stock.Position;

                        myy.grosssal = stock.grosssal;

                        myy.netsalary = stock.netsalary;


                        myy.username = stock.username;

                        myy.refrr = "APPROVED";

                        myy.dateCreated = stock.dateCreated;
                        myy.action = "APPROVED";

                        if (validate == "SENT")
                        {
                            insertUpdateDataStatic(myy, path);
                        }

                    }
                    catch (Exception)
                    {

                        continue;
                    }
                }
            }
            catch (Exception)
            {


            }


            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Static have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());


        }
        public int NumberA(string s, string path)
        {
            int ss = 0;
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<StaticDetails>().Where(v => v.CustomerNumber == s);

            foreach (var d in query)
            {
                ss=d.Id;
            }
            return ss;
        }
        public int Number(string s,string path)
        {
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<StaticDetails>().Where(v=>v.CustomerNumber==s);

            
            return query.Count();
        }
        public void Customers()
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("customers/{users}", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request.AddUrlSegment("users", MainActivity.username.ToString().Replace(" ", "").Replace("*", "-"));

            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Customers> dataList = JsonConvert.DeserializeObject<List<Customers>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                Customer nn = new Customer();
                nn.Fullnames = p.Fullnames;
                nn.CustomerNumber = p.CustomerNumber;
                try
                {
                    insertUpdateDataCustomers(nn, path);
                }
                catch (Exception)
                {

                    continue;
                }
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Customers have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);

        }
        public void Banks()
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("Bank", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<String> dataList = JsonConvert.DeserializeObject<List<String>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                Bank nn = new Bank();
                nn.bankname = p.ToString();
                insertUpdateDataBank(nn, path);
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Banks have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

        }

        public void Branches()
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("Branch", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Branchs> dataList = JsonConvert.DeserializeObject<List<Branchs>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                Branch nn = new Branch();
                nn.bankname = p.Bank;
                nn.branchcode = p.Branchcode;
                nn.branchname = p.Branch;
                int me = insertBranch(p.Bank, p.Branchcode, p.Branch);
                if (me==0)
                {
                insertUpdateDataBranch(nn, path);
                }
                
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Branchs have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

        }
        public int insertBranch(string BankName,string Branchcode,string Branchname)
        {
            var db = new SQLiteConnection(path);
            // this counts all records in the database, it can be slow depending on the size of the database
            ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
            var query = db.Table<Branch>();

            foreach(var v in query)
            {
                if ((v.bankname+""+v.branchcode+v.branchname).ToLower().Replace(" ","").ToString()== (BankName + "" + Branchcode + Branchname).ToLower().Replace(" ","").ToString())
              
                {
                   return 1;
                }
                
     
            }
            return 0;
        }
        public void clienttypes()
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("clients", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<String> dataList = JsonConvert.DeserializeObject<List<String>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                clienttype nn = new clienttype();
                nn.clienttypename = p.ToString();
                insertUpdateDataClients(nn, path);
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Clients have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

        }

        public void Purposes(){
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("purpose", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Purposes> dataList = JsonConvert.DeserializeObject<List<Purposes>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                Purpose nn = new Purpose();
                nn.purposeN = p.pepname;
                insertUpdateDataPurpose(nn, path);
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Purposes have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

        }

        public void Products()
        {
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("Products", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Productss> dataList = JsonConvert.DeserializeObject<List<Productss>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                Products nn = new Products();
                nn.DisplayName = p.DisplayName;
                nn.Code = p.Code;
                insertUpdateDataProducts(nn, path);
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Products have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

        }
        public void sector() {

            //call rest api
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("Sector", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<String> dataList = JsonConvert.DeserializeObject<List<String>>(validate);

            var dbsel = from s in dataList
                        select s;

            foreach (var p in dbsel)
            {
                Sector nn=new Sector();
                nn.sectorname = p.ToString();
                insertUpdateData(nn,path);
            }
            Notification.Builder notificationBuilder = new Notification.Builder(this)
     .SetSmallIcon(Resource.Drawable.qqsmall)
     .SetContentTitle("DATA UPDATES")
     .SetContentText("Sectors have been updated");

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

        }

        private string insertUpdateData(Sector data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        private string insertUpdateDataClients(clienttype data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        private string insertUpdateDataBank(Bank data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        private string insertUpdateDataBranch(Branch data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        private string insertUpdateDataPurpose(Purpose data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        private string insertUpdateDataProducts(Products data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        private string insertUpdateDataCustomers(Customer data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        private string insertUpdateDataLoans(LoanApproval data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.UpdateAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        private string insertUpdateDataStatic(StaticDetails data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.UpdateAsync(data);
                return "Updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        private string insertUpdateDataStaticA(StaticDetails data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
    }
}