﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
namespace CMS.Model
{
    public class Amortization
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string loanID { get; set; }
        public decimal loanamount { get; set; }
        public DateTime paymentdate { get; set; }
        public decimal monthly { get; set; }
        public string custno { get; set; }
        [Unique]
        public string refs { get; set; }

    }
    public class Amortizations
    {
        public string loanID { get; set; }
        public string loanamount { get; set; }
        public string paymentdate { get; set; }
        public string monthly { get; set; }
        public string custno { get; set; }
        public string refs { get; set; }

    }
}