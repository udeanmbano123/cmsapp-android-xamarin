﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace CMS.Model
{
    public class Branch
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string branchname { get; set; }
        public string bankname { get; set; }
        public string branchcode { get; set; }
    }

    public class Branchs
    {
        
        public string Branch { get; set; }
        public string Bank{ get; set; }
        public string Branchcode { get; set; }
    }
}