﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using CMS.Model;
using CMS;

namespace OldMutualAndroid.Model
{
    class CustomListQ : BaseAdapter
    {
        private Activity context;
        public List<string> listitem;

        private int[] imageId;
        //public override int Count
        //{
        //    get
        //    {
        //        return listitem.Count;
        //    }
        //}

        public CustomListQ(Activity context, List<string> listitem, int[] imageId)
        {
            this.context = context;
            this.listitem = listitem;


            this.imageId = imageId;
        }
        public override int Count
        {
            get { return listitem.Count; }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public string GetItemAtPosition(int position)
        {
            return listitem[position];
        }

        public String GetItemAtPositionId(int position)
        {
            return listitem[position].ToString();
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listitem[position];
            var view = context.LayoutInflater.Inflate(Resource.Layout.Search, parent, false);
            TextView txtTitle = (TextView)view.FindViewById(Resource.Id.txt);
            ImageView imageView = (ImageView)view.FindViewById(Resource.Id.img);
            txtTitle.SetText(item.ToString().ToUpper(), TextView.BufferType.Normal);
            imageView.SetImageResource(imageId[position]);
            return view;
        }
    }
}