using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using CMS.Model;
using CMS;

namespace OldMutualAndroid.Model
{
    class CustomListSMR : BaseAdapter
    {
        private Activity context;
        public List<Repayments> listitem;
      
        private Drawable[] imageId;
        //public override int Count
        //{
        //    get
        //    {
        //        return listitem.Count;
        //    }
        //}
    
        public CustomListSMR(Activity context, List<Repayments> listitem,Drawable[] imageId)
        {
            this.context = context;
            this.listitem = listitem;
  

            this.imageId = imageId;
        }
        public override int Count
        {
            get { return listitem.Count; }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public Repayments GetItemAtPosition(int position)
        {
            return listitem[position];
        }

        public String GetItemAtPositionId(int position)
        {
            return listitem[position].loanID.ToString();
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listitem[position];
            var view = context.LayoutInflater.Inflate(Resource.Layout.list_singleAM, parent, false);
            TextView loanno = (TextView)view.FindViewById(Resource.Id.loann);
            //TextView firstpay = (TextView)view.FindViewById(Resource.Id.firstpay);
            TextView lastpay = (TextView)view.FindViewById(Resource.Id.lastpay);
            TextView loanamount = (TextView)view.FindViewById(Resource.Id.loanamount);
            TextView monthlypay = (TextView)view.FindViewById(Resource.Id.monthlypay);
            TextView custno = (TextView)view.FindViewById(Resource.Id.custno);
            ImageView imageView = (ImageView)view.FindViewById(Resource.Id.img);
            loanno.SetText("LOAN NUMBER:"+ item.loanID, TextView.BufferType.Normal);
            lastpay.SetText("PAYMENT DATE:" + Convert.ToDateTime(item.paymentdate).ToString("dd-MMM-yyyy"), TextView.BufferType.Normal);
            loanamount.SetText("LOAN AMOUNT : ZMW" + Convert.ToDecimal(item.loanamount).ToString("#,##0.00"), TextView.BufferType.Normal);
            monthlypay.SetText("PAYMENT : ZMW" + Convert.ToDecimal(item.monthly).ToString("#,##0.00"), TextView.BufferType.Normal);
            custno.SetText("NAMES :"+ item.custno.ToString(), TextView.BufferType.Normal);
           
            imageView.SetImageDrawable(imageId[position]);
            return view;
        }
    }
}