﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CMS.Model
{
    public class LoanStatement
    {
       public DateTime TrxnDate { get; set; }
      public decimal Amount { get; set; }
      public string Refrence { get; set; }
     public string Description {get;set;}

        public decimal Balance { get; set; }
    }
    public class LoanAmortization
    {
        public string loanno { get; set; }
        public DateTime firstpay { get; set; }
        public DateTime lastpay { get; set; }
        public decimal loanamount { get; set; }
        public decimal monthlypay { get; set; }
        public string custno { get; set; }
       
    }
}