﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CMS.Model
{
   public class LoanStatus
    {
        public string ID { get; set; }
        public string STATUS { get; set; }

        public decimal AMOUNT { get; set; }

        public DateTime CREATED_DATE { get; set; }
    }
}