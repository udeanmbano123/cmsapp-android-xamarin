﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
namespace CMS.Model
{
    public class Loans
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string membernumber { get; set; }
        //guarantor
        public string name { get; set; }

        public string IDNo { get; set; }

        public string PhoneNo { get; set; }

        public string Relationship { get; set; }

        public string Address { get; set; }


        public DateTime dob{ get; set; }

        //loan
        public string product { get; set; }
        public decimal loanamounty { get; set; }
        
        public decimal recamount { get; set; }
        
        public string repaysource { get; set; }
        
        public string loanpurpose { get; set; }
        
        public DateTime frepay { get; set; }
        
        public DateTime appdate { get; set; }

        //employment information
        public string currentemp { get; set; }
        public string empaddress { get; set; }

        public DateTime? dateemp { get; set; }

        public string Supervisor { get; set; }

        public string Phone { get; set; }

        public string Position { get; set; }

        public decimal grosssal { get; set; }

        public decimal netsalary { get; set; }

        public string username { get; set; }

        public string refrr { get; set; }

        public string dateCreated { get; set; }

        public string status { get; set; }
        public string loanstatus { get; set; }

        public string loanNr { get; set; }

    }
    public class LoansD
    {
        public string membernumber { get; set; }
        //guarantor
        public string name { get; set; }
        public string curraddr { get; set; }
        

        public string phone { get; set; }

        public string rel { get; set; }
        public string id { get; set; }
        

        public string dob { get; set; }

        //loan
        
        public string loanamount { get; set; }

      public string purpose { get; set; }
        public string recamount { get; set; }

        public string sourcerep { get; set; }

        

        public string repaydte { get; set; }

        public string appldate { get; set; }

        public string prod { get; set; }

        //employment information

        public string user { get; set; }
        public string curremployer { get; set; }
        public string empaddr { get; set; }

      

        public string phoneno { get; set; }

        public string position { get; set; }

        public string gross { get; set; }

        public string netsal { get; set; }
        public string empdate { get; set; }
        public string loanstatus { get; set; }

        public string loanNr { get; set; }

    }
}