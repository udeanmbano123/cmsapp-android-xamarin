﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace CMS.Model
{
    public class StaticDetails
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Unique]
        public string CustomerNumber { get; set; }

        public string clienttype { get; set; }

        public string applicationtype { get; set; }

        public string Surname { get; set; }

        public string Forenames { get; set; }

        public string NRC { get; set; }

        public string sector { get; set; }

        public DateTime dsteofbirth { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string area { get; set; }
        public string gender { get; set; }

        public string martialstatus { get; set; }

        public string phonenumber { get; set; }

        //spouse
        public string spousename { get; set; }
        public string spousephone { get; set; }

        public string Occupation { get; set; }

        public int numberofdependants { get; set; }

        //Bsanking
        public string Bank { get; set; }
        public string Branch { get; set; }

        public string AccountNumber { get; set; }

        public string branchcode { get; set; }

        //employer
        public string currentemp { get; set; }
        public string empaddress { get; set; }

        public string dateemp { get; set;}

        public string Supervisor { get; set; }

        public string Phone { get; set; }

        public string Position { get; set; }

        public decimal grosssal { get; set; }

        public decimal netsalary { get; set; }

     
        public string username { get; set; }

        public string refrr { get;set;}

        public DateTime dateCreated { get; set; }
        public string action { get; set; }

    }

    public class StaticDetailsUpload
    {
        public string customernumber { get; set; }

        public string clienttype { get; set; }

        public string applicatiotype { get; set; }

        public string surname { get; set; }

        public string forenames { get; set; }

        public string id { get; set; }

        public string sector { get; set; }

        public DateTime dob { get; set; }

        public string address { get; set; }

        public string City { get; set; }

        public string area { get; set; }
        public string gender { get; set; }

        public string martial { get; set; }

        public string phoneno { get; set; }

        //spouse
        public string spname { get; set; }
        public string spphone { get; set; }

        public string occup { get; set; }

        public string numdep{ get; set; }

        //Bsanking
        public string bank { get; set; }
        public string branch { get; set; }

        public string account { get; set; }

        public string branchcode { get; set; }

        //employer
        public string currentemp { get; set; }
        public string empaddr { get; set; }

        public string empdate { get; set; }

        public string superv { get; set; }

        public string Phone { get; set; }

        public string Position { get; set; }

        public string gross { get; set; }

        public string netsal { get; set; }


          }
}