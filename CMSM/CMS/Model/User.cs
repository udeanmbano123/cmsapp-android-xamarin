﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
namespace CMS.Model
{

      public  class User
    {
        [PrimaryKey, AutoIncrement]
        public int UserId { get; set; }
        public string username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public Boolean AccountVerified{get;set;}

        public string usertype { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}