using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;

using CMS.Fragments;
using Android.Support.V7.App;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using CMS.Model;
using SQLite;
using System.IO;

namespace CMS.Activities
{
    [Activity(Label = "Credit Management Loan Officer", MainLauncher =false, LaunchMode = LaunchMode.SingleTop, Icon = "@drawable/Icon")]
    public class MainActivity : BaseActivity
    {

        DrawerLayout drawerLayout;
        NavigationView navigationView;

        public static string membernumber { get; set; }
        public static string memberid { get; set; }
        public static string branch { get; set; }
        public static string username { get; set; }

        public static string loanno { get; set; }
        public static decimal loanbalance { get; set; }

        public static string baseUrl { get; set; }
        TextView jump;
        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.main;
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            membernumber = Intent.GetStringExtra("Member") ?? "Data not available";
            branch = Intent.GetStringExtra("Branch") ?? "Data not available";
            username = Intent.GetStringExtra("Username") ?? "Data not available";
            ////membernumber = Intent.GetStringExtra("Member") ?? "Data not available";
            //branch = Intent.GetStringExtra("Branch") ?? "Data not available";
            //username =
            drawerLayout = this.FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
           jump = this.FindViewById<TextView>(Resource.Id.lender);
           jump.Text =membernumber+" , "+username;
           baseUrl = "http://166.63.10.232/CMSSApi";
            //Set hamburger items menu
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            //setup navigation view
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            //check for vali account

            //update banks
            string path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");
            


            //handle navigation
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.home:
                        ListItemClicked(0);
                        break;
                    case Resource.Id.create:
                        ListItemClicked(1);
                        break;
                    case Resource.Id.existing:
                        ListItemClicked(2);
                        break;
                    case Resource.Id.loanapp:
                        ListItemClicked(3);
                        break;
                    case Resource.Id.amor:
                        ListItemClicked(4);
                        break;
                    case Resource.Id.repay:
                        ListItemClicked(5);
                        break;
                    case Resource.Id.statement:
                        ListItemClicked(6);
                        break;
                    case Resource.Id.aboutmfi:
                        ListItemClicked(7);
                        break;
                    case Resource.Id.contact:
                        ListItemClicked(8);
                        break;
                    case Resource.Id.prod:
                        ListItemClicked(9);
                        break;
                    case Resource.Id.changepassword:
                        ListItemClicked(10);
                        break;
                    case Resource.Id.logout:
                        ListItemClicked(11);
                        break;
                }

                try
                {
                    Snackbar.Make(drawerLayout, "You selected: " + e.MenuItem.TitleFormatted, Snackbar.LengthLong)
                                .Show();
                }
                catch (System.Exception)
                {
                    
                }
                if (e.MenuItem.TitleFormatted.ToString() == "Home")
                {
                    Android.Support.V4.App.Fragment fragment = null;
                    SupportFragmentManager.BeginTransaction()
                      .Replace(Resource.Id.content_frame, Fragment1.NewInstance())
                      .Commit();
                }
               
                drawerLayout.CloseDrawers();
            };


            //if first time you will want to go ahead and click first item.
            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
              
                    ListItemClicked(0);
                    Android.Support.V4.App.Fragment fragment = null;
                    fragment = Fragment1.NewInstance();
                    SupportFragmentManager.BeginTransaction()
                   .Replace(Resource.Id.content_frame, fragment)
                   .Commit();
               
            }
        }

        int oldPosition = -1;
        private void ListItemClicked(int position)
        {
            //this way we don't load twice, but you might want to modify this a bit.
            if (position == oldPosition)
                return;

            oldPosition = position;

            Android.Support.V4.App.Fragment fragment = null;
            switch (position)
            {
                case 0:
                    fragment = Fragment1.NewInstance();
                    break;
                case 1:
                    fragment = Fragment2.NewInstance();
                    break;
                case 2:
                    fragment = CustomersL.NewInstance();
                    break;
                case 3:
                  
                    fragment = CustomersLL.NewInstance();
                    
                    break;
                case 4:

                    fragment = Fragment1.NewInstance();

                    break;
                case 5:

                    fragment = Fragment1.NewInstance();

                    break;
                case 6:

                    fragment = Fragment1.NewInstance();

                    break;
                case 7:

                    fragment = Fragment1.NewInstance();

                    break;
                case 8:

                    fragment = Fragment1.NewInstance();

                    break;
                case 9:

                    fragment = Fragment1.NewInstance();

                    break;
                case 10:

                    fragment = Fragment1.NewInstance();

                    break;
                case 11:
                    {
                        Finish();
                        StartActivity(typeof(LoginActivity));
                    }
                    break;
            }

            try
            {
                SupportFragmentManager.BeginTransaction()
                   .Replace(Resource.Id.content_frame, fragment).Detach(fragment).Attach(fragment)
                   .Commit();
            }
            catch (System.Exception)
            {

            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
        private string insertUpdateData(User data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        
    }
}

