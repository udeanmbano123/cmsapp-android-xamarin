﻿using Android.App;
using Android.OS;
using Android.Widget;
using CMS.Model;
using RestSharp;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace CMS.Activities
{
    [Activity(Label = "Credit Managment Registration", MainLauncher = false, Icon = "@drawable/icon")]
    public class RegisterActivity : Activity
    {
        //int count = 1;
         Button button, button2,button3;
        EditText username, member, pass, pass2;
        public string path;
        Spinner usertype;
        private List<string> usertypes= new List<string>();
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
             
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Register);
            baseUrl = "http://166.63.10.232/CMSApp";
            button = FindViewById<Button>(Resource.Id.btnLogin);
            button2 = FindViewById<Button>(Resource.Id.btnClear);
            button3 = FindViewById<Button>(Resource.Id.btnBack);
            username = FindViewById<EditText>(Resource.Id.username);
            usertype = FindViewById<Spinner>(Resource.Id.spinner1);
            usertypes.Add("Please select account type");
            usertypes.Add("Loan Approver");
            usertypes.Add("Loan Officer");
            var adapter2 = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerItem, usertypes);

            usertype.Adapter = adapter2;
       
            pass = FindViewById<EditText>(Resource.Id.password);
           pass2 = FindViewById<EditText>(Resource.Id.confirmpassword);
            //member = FindViewById<EditText>(Resource.Id.member);
            //email = FindViewById<EditText>(Resource.Id.email);

            button2.Click += delegate
            {

                username.Text = "";
                pass.Text = "";
                username.Text = "";
                //member.Text = "";
                pass.Text = "";
                pass2.Text = "";
                //email.Text = "";
            };

            button3.Click += delegate
            {

                StartActivity(typeof(LoginActivity));
            };
            button.Click += LoginClick;
        }

      public void LoginClick(object sender, EventArgs e)
        {
            //username = FindViewById<EditText>(Resource.Id.username);
            //pass = FindViewById<EditText>(Resource.Id.password);
            //pass2 = FindViewById<EditText>(Resource.Id.confirmpassword);
            //member = FindViewById<EditText>(Resource.Id.member);
            //email = FindViewById<EditText>(Resource.Id.email);

            bool validA = username.Text.Replace(" ","").All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));
            if (username.Text == "")
            {
                Toast.MakeText(this, "Username  is required", ToastLength.Short).Show();
                return;

            }
            else if (validA==false)
            {
                Toast.MakeText(this, "Username must be a letter , digit or underscore", ToastLength.Short).Show();
                return;

            }
            else if (pass.Text == "")
            {
                Toast.MakeText(this, "Password is required", ToastLength.Short).Show();
                return;

            }
            else if (pass2.Text == "")
            {
                Toast.MakeText(this, "Confirm password  is required", ToastLength.Short).Show();
                return;

            }else if (pass.Text != pass2.Text)
             {
                    Toast.MakeText(this, "Passwords  do not match", ToastLength.Short).Show();
                    return;

            }
            else if (usertype.SelectedItem.ToString()=="Please select account type")
            {
                Toast.MakeText(this, "Please select account", ToastLength.Short).Show();
                return;

            }
            else
            {
            
                User my = new User();
                my.username = username.Text;
                my.Password = ComputeHash(pass.Text, new SHA256CryptoServiceProvider());
                my.ConfirmPassword = ComputeHash(pass2.Text, new SHA256CryptoServiceProvider());
                my.CreateDate = DateTime.Now;
                my.AccountVerified = false;
                my.usertype = usertype.SelectedItem.ToString();
                path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");

                string val = insertUpdateData(my,path);
                if(val=="Inserted")
                {
                    //Toast.MakeText(this, "Registration was succesful", ToastLength.Short).Show();
                    Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                    Android.App.AlertDialog alertDialog = builder.Create();
                    alertDialog.SetTitle("Notification");
                    alertDialog.SetIcon(Resource.Drawable.ic_launcher);
                    alertDialog.SetMessage("Registration was successful");

                   
                    alertDialog.SetButton("OK", (s, ev) =>
                    {
                        //DO Something

                       
                    });
                    alertDialog.Show();
                }
                else
                {
                    //Toast.MakeText(this, "Registration was unsuccesful", ToastLength.Short).Show();
                    Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                    Android.App.AlertDialog alertDialog = builder.Create();
                    alertDialog.SetTitle("Notification");
                    alertDialog.SetIcon(Resource.Drawable.ic_launcher);
                    alertDialog.SetMessage("Registration was unsuccessful");


                    alertDialog.SetButton("OK", (s, ev) =>
                    {
                        //DO Something


                    });
                    alertDialog.Show();
                }

                
                username.Text = "";
                pass.Text = "";
                username.Text = "";
                //member.Text = "";
                pass.Text = "";
                pass2.Text = "";
                //email.Text = "";
            }

        }

        private string insertUpdateData(User data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
      
    }
}

