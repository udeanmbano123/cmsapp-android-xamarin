using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CMS.Activities
{
    using System.Threading;

    using Android.App;
    using Android.OS;
    using Activities;
    using Android.Content.PM;

   [Activity(Theme = "@style/MyTheme.Splash",MainLauncher =true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
    ScreenOrientation = ScreenOrientation.Portrait)]
    public class Splash : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Thread.Sleep(4000); // Simulate a long loading process on app startup.
            StartActivity(typeof(LoginActivity));
        }
    }
}