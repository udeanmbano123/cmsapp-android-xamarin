      using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using CMS.Activities;
using CMS.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using static Android.Widget.TabHost;

namespace CMS.Fragments
{
    public class Fragment2UA : Fragment
    {
        private List<string> clientypes = new List<string>();
        Spinner clients;
        private List<string> bank = new List<string>();
        Spinner banks;
        private List<string> branch = new List<string>();
        Spinner branchs;
        private List<string> sector = new List<string>();
        Spinner sectors;
        private List<string> area = new List<string>();
        Spinner areas;
        string path;
        TextView _dateDisplay;
       Button _dateSelectButton;
        TextView _dateDisplay2;
        Button _dateSelectButton2;
        Button btnGo;
        public EditText surname, forenames, nrcc, secc,addreress, city, areaz, gender, martial, spousename;
        public EditText spousephone, spouseoccu, numberofdep, bankl, branchl, accountnumber, branchcode;
        public EditText curremployer, empaddr, suprvme, empphone, empPosition, gross, netsal,phoneno,member;
        public TextView dob, emdate;
        public RadioButton app1, app2, app3, app4;
        public RadioButton gen1, gen2;
        public RadioButton ma1, mar2, mar3, mar4;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

       
        public static Fragment2UA NewInstance()
        {
            var frag2 = new Fragment2UA { Arguments = new Bundle() };
            return frag2;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");

            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

                 View view= inflater.Inflate(Resource.Layout.StaticsU, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Static Details");
            spec1.SetContent(Resource.Id.tab1);
            spec1.SetIndicator("Static Details");

            TabSpec spec2 = tabHost.NewTabSpec("Spouse Details");
            spec2.SetIndicator("Spouse Details");
            spec2.SetContent(Resource.Id.tab2);

            TabSpec spec3 = tabHost.NewTabSpec("Bank Details");
            spec3.SetIndicator("Bank Details");
            spec3.SetContent(Resource.Id.tab3);

            TabSpec spec4 = tabHost.NewTabSpec("Employment Information");
            spec4.SetIndicator("Employment Information");
            spec4.SetContent(Resource.Id.tab4);

            tabHost.AddTab(spec1);
            tabHost.AddTab(spec2);
            tabHost.AddTab(spec3);
            tabHost.AddTab(spec4);

            //binding lists
            //public string clientstype, apptype, surname, forenames, nrcc, secc, dob, addreress, city, areaz, gender, martial, spousename
            // public string ;
            // public string curremployer, empaddr, emdate, suprvme, empphone, empPosition, gross, netsal;

            surname = (EditText)view.FindViewById(Resource.Id.surname);
            forenames = (EditText)view.FindViewById(Resource.Id.forenames);
            nrcc = (EditText)view.FindViewById(Resource.Id.nrc);
            dob = (TextView)view.FindViewById(Resource.Id.dateDisplay);
          addreress = (EditText)view.FindViewById(Resource.Id.address);
            city = (EditText)view.FindViewById(Resource.Id.city);
         spousename = (EditText)view.FindViewById(Resource.Id.spouse);
            spousephone = (EditText)view.FindViewById(Resource.Id.spousephone);
            spouseoccu =(EditText)view.FindViewById(Resource.Id.spouseoccupation);
            numberofdep = (EditText)view.FindViewById(Resource.Id.spousedep);
          accountnumber = (EditText)view.FindViewById(Resource.Id.accountnumber);
            branchcode = (EditText)view.FindViewById(Resource.Id.branchcode);
            branchcode.Enabled = false;
            curremployer = (EditText)view.FindViewById(Resource.Id.curremployee);
            empaddr = (EditText)view.FindViewById(Resource.Id.empaddress);
            emdate = (TextView)view.FindViewById(Resource.Id.dateDisplay2);
            suprvme = (EditText)view.FindViewById(Resource.Id.supervisor);
            empphone = (EditText)view.FindViewById(Resource.Id.phone);
            empPosition = (EditText)view.FindViewById(Resource.Id.empPosition);
            gross = (EditText)view.FindViewById(Resource.Id.grosssalary);
            netsal = (EditText)view.FindViewById(Resource.Id.netsalary); 
            phoneno= (EditText)view.FindViewById(Resource.Id.phoneno);
            clients = (Spinner)view.FindViewById(Resource.Id.sbu);
         banks = (Spinner)view.FindViewById(Resource.Id.drpbank);
            branchs = (Spinner)view.FindViewById(Resource.Id.drpbranch);
            sectors = (Spinner)view.FindViewById(Resource.Id.drpsector);
            areas = (Spinner)view.FindViewById(Resource.Id.drparea);
        //radio button groups
        app1= (RadioButton)view.FindViewById(Resource.Id.PMEC);
        app2=(RadioButton)view.FindViewById(Resource.Id.Bankers);
        app3=(RadioButton)view.FindViewById(Resource.Id.PDA);
            app4 = (RadioButton)view.FindViewById(Resource.Id.Other);
     gen1=(RadioButton)view.FindViewById(Resource.Id.ml);
            gen2=(RadioButton)view.FindViewById(Resource.Id.fml);
  ma1=(RadioButton)view.FindViewById(Resource.Id.mm1);
            mar2 = (RadioButton)view.FindViewById(Resource.Id.mm2);
            mar3=(RadioButton)view.FindViewById(Resource.Id.mm3);
            mar4=(RadioButton)view.FindViewById(Resource.Id.mm4);
         member =(EditText)view.FindViewById(Resource.Id.member);
            member.Enabled = false;
           
            int sam = Convert.ToInt32(MainActivity.memberid);
            _dateDisplay = view.FindViewById<TextView>(Resource.Id.dateDisplay);
            _dateSelectButton = view.FindViewById<Button>(Resource.Id.dateSelectButton);
            _dateSelectButton.Click += _dateSelectButton_Click;
            _dateDisplay2 = view.FindViewById<TextView>(Resource.Id.dateDisplay2);
            _dateSelectButton2 = view.FindViewById<Button>(Resource.Id.dateSelectButton2);
            _dateSelectButton2.Click += _dateSelectButton2_Click;
            //query data
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                var query = db.Table<StaticDetails>().Where(v => v.Id==sam);

                foreach (var stock in query)
                {
                   
                    
                    clientypes.Add(stock.clienttype);
                    bank.Add(stock.Bank);
                    sector.Add(stock.sector);
                    area.Add(stock.area);
                    if (stock.Branch == null)
                    {
                        branch.Add("--Select Branch--");
                    }else
                    {
                     branch.Add(stock.Branch);
                    }
                    
                    if (stock.applicationtype=="PMEC")
                    {
                        app1.Checked = true;
                    }
                    else if (stock.applicationtype == "Bankers")
                    {
                        app2.Checked = true;
                    }
                    else if (stock.applicationtype == "PDAs")
                    {
                        app3.Checked = true;
                    }
                    else if (stock.applicationtype == "Other")
                    {
                        app4.Checked = true;
                    }

                    if (stock.gender== "MALE")
                    {
                        gen1.Checked = true;
                    }
                    else if (stock.gender == "FEMALE")
                    {
                        gen2.Checked = true;
                    }
                    if (stock.martialstatus == "DIVORCED")
                    {
                        ma1.Checked = true;
                    }
                    else if (stock.martialstatus == "SINGLE")
                    {
                        mar2.Checked = true;
                    }
                    else if (stock.martialstatus == "MARRIED")
                    {
                        mar3.Checked = true;
                    }
                    else if (stock.martialstatus == "WIDOWED")
                    {
                        mar4.Checked = true;
                    }

                    member.Text=stock.CustomerNumber;
                    surname.Text=stock.Surname;
                   forenames.Text=stock.Forenames;
                   nrcc.Text=stock.NRC;
                    dob.Text=stock.dsteofbirth.ToString();
                     addreress.Text=stock.Address;
                    city.Text=stock.City ;
                     phoneno.Text=stock.phonenumber;
                    spousename.Text=stock.spousename;
                     spousephone.Text=stock.spousephone;
                     spouseoccu.Text=stock.Occupation;
                    try
                    {
                         numberofdep.Text=stock.numberofdependants.ToString();
                    }
                    catch (Exception f)
                    {

                        numberofdep.Text = "0";

                    }
                    accountnumber.Text=stock.AccountNumber;
                    branchcode.Text=stock.branchcode;
                    curremployer.Text=stock.currentemp;
                    empaddr.Text=stock.empaddress;
                    emdate.Text= stock.dateemp;
                    suprvme.Text=stock.Supervisor;
                    empphone.Text=stock.Phone;
                    empPosition.Text=stock.Position;
                   gross.Text = stock.grosssal.ToString();
                    netsal.Text=stock.netsalary.ToString();
                  
                }


                // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            clientypes.Add("--Select Client Type--");
            bank.Add("--Select Bank--");
            sector.Add("--Select Sector--");
            area.Add("--Select Area--");
            var clientsz = Clients(path);
            foreach (var z in clientsz)
            {
                clientypes.Add(z.ToString());
            }
      
            var banksz = Bankss(path);
            foreach (var z in banksz)
            {
                bank.Add(z.ToString());
            }
           
            
            var sectorsz = Sectors(path);
            foreach (var z in sectorsz)
            {
                sector.Add(z.ToString());
            }
            
           area.Add("Urban");
            area.Add("Periurban");
            area.Add("Rural");
            var adapter1 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, clientypes);

            clients.Adapter = adapter1;

            var adapter2 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, bank);

            banks.Adapter = adapter2;

            
            var adapter4 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, sector);

            sectors.Adapter = adapter4;
            var adapter5 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, area);

            areas.Adapter = adapter5;

 
            banks.ItemSelected += Banks_ItemSelected;
            branchs.ItemSelected += Branchs_ItemSelected;
            btnGo = view.FindViewById<Button>(Resource.Id.btnLogin);
            btnGo.Click += BtnGo_Click;
            btnGo.Text = "APPROVE";
            return view;
        }

        private void BtnGo_Click(object sender, EventArgs e)
        {
            //Toast.MakeText(this.Activity, "Record Updated successfully", ToastLength.Long).Show();

            string clint = "";
            string appc = "";
            string genn = "";
            string martial = "";
            if (app1.Checked == true)
            {
                appc = "PMEC";
            }
            else if (app2.Checked == true)
            {
                appc = "Bankers";
            }
            else if (app3.Checked == true)
            {
                appc = "PDAs";
            }
            else if (app4.Checked == true)
            {
                appc = "Other";
            }

            if (gen1.Checked == true)
            {
                genn = "MALE";
            }
            else if (gen2.Checked == true)
            {
                genn = "FEMALE";
            }
            if (ma1.Checked == true)
            {
                martial = "DIVORCED";
            }
            else if (mar2.Checked == true)
            {
                martial = "SINGLE";
            }
            else if (mar3.Checked == true)
            {
                martial = "MARRIED";
            }
            else if (mar4.Checked == true)
            {
                martial = "WIDOWED";
            }
            if (appc =="")
            {
                Toast.MakeText(this.Activity, "Application type is required", ToastLength.Long).Show();

            }else if (clients.SelectedItem.ToString()=="--Select Client Type--")
            {
          
                Toast.MakeText(this.Activity, "Client type is required", ToastLength.Long).Show();

            }
            else if (surname.Text== "")
            {
                Toast.MakeText(this.Activity, "Client type is required", ToastLength.Long).Show();

            }
            else if (forenames.Text == "")
            {
                Toast.MakeText(this.Activity, "Forenames is required", ToastLength.Long).Show();

            }
            else if (nrcc.Text == "")
            {
                Toast.MakeText(this.Activity, "NRC is required", ToastLength.Long).Show();

            }
            else if (dob.Text == "")
            {
                Toast.MakeText(this.Activity, "Date of birth is required", ToastLength.Long).Show();

            }
            else if (addreress.Text == "")
            {
                Toast.MakeText(this.Activity, "Address is required", ToastLength.Long).Show();

            }
            else if (city.Text == "")
            {
                Toast.MakeText(this.Activity, "City is required", ToastLength.Long).Show();

            }
            else if (areas.SelectedItem.ToString() == "--Select Area--")
            {
                
                Toast.MakeText(this.Activity, "Area is required", ToastLength.Long).Show();

            }
            else if (sectors.SelectedItem.ToString() == "--Select Sector--")
            {

                Toast.MakeText(this.Activity, "Sector is required", ToastLength.Long).Show();

            }  else if (martial == "")
            {
                Toast.MakeText(this.Activity, "Martial status is required", ToastLength.Long).Show();

            }else
            {
                //insert data
                StaticDetails my = new Model.StaticDetails();
                int sam = Convert.ToInt32(MainActivity.memberid);
                my.Id = sam;
                my.CustomerNumber =member.Text;
                my.clienttype = clients.SelectedItem.ToString();
                my.applicationtype = appc;
                my.Surname = surname.Text;
                my.Forenames = forenames.Text;
                my.NRC = nrcc.Text;
                my.sector = sectors.SelectedItem.ToString();
                my.dsteofbirth =Convert.ToDateTime(dob.Text);
                my.Address = addreress.Text;
                my.City = city.Text;
                my.area = areas.SelectedItem.ToString();
                my.gender = genn;
                my.martialstatus = martial;
                my.phonenumber = phoneno.Text;
                my.spousename = spousename.Text;
                my.spousephone = spousephone.Text;
                my.Occupation = spouseoccu.Text;
               try 
	{	        
		 my.numberofdependants = Convert.ToInt32(numberofdep.Text);
	}
	catch (Exception f)
	{

                    my.numberofdependants = 0;

    }
                try 
	{	        
		my.Bank = banks.SelectedItem.ToString();
	}
	catch (Exception)
	{
                    my.Bank = "None";

                }
                try
                {
 my.Branch = branchs.SelectedItem.ToString();
              
                }
                catch (Exception)
                {

                    my.Branch = "None";
                }
                 my.AccountNumber = accountnumber.Text;
                my.branchcode = branchcode.Text;
                my.currentemp = curremployer.Text;
                my.empaddress = empaddr.Text;
                my.dateemp = emdate.Text;
                my.Supervisor = suprvme.Text;
                my.Phone = empphone.Text;
                my.Position = empPosition.Text;
                my.grosssal = Convert.ToDecimal(gross.Text);
                my.netsalary = Convert.ToDecimal(netsal.Text);
                my.username = MainActivity.username;

                my.refrr = "UPDATE";

                my.dateCreated = DateTime.Now;
                my.action = "UPDATE";

               string todo= insertUpdateData(my, path);

                if (todo== "Updated")
                {
                    Toast.MakeText(this.Activity, "Record was updated successfully", ToastLength.Long).Show();
                    //back to search list
                    var trans2 = this.FragmentManager.BeginTransaction();
                    trans2.Replace(Resource.Id.content_frame, CustomersL.NewInstance(), "Fragment2U");
                    trans2.AddToBackStack("Fragment2U");
                    trans2.Commit();
                }
                else
                {
                    Toast.MakeText(this.Activity, "Record was not updated successfully", ToastLength.Long).Show();

                }
            }
}
        private string insertUpdateData(StaticDetails data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
              
                db.UpdateAsync(data);
                return "Updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }


        private void Branchs_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner rb = (Spinner)sender;
            branchcode.Text= BranchesC(path, rb.SelectedItem.ToString(),banks.SelectedItem.ToString());
        }

        private void Banks_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            //branch.Clear();
            Spinner rb = (Spinner)sender;
            branch.Add("--Select A Branch--");
            var branchsz = Branches(path, rb.SelectedItem.ToString());
            foreach (var z in branchsz)
            {
                branch.Add(z.ToString());
            }
            var adapter3 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, branch);

            branchs.Adapter = adapter3;
        }

        private void _dateSelectButton2_Click(object sender, System.EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay2.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }
        private List<string> Bankss(string path)
        {
            List<string> my = new List<string>();
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                var query = db.Table<Bank>().Where(v => v.bankname!="");

                foreach (var stock in query)
                    my.Add(stock.bankname);

               // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return my;
        }
        private List<string> Clients(string path)
        {
            List<string> my = new List<string>();
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                var query = db.Table<clienttype>().Where(v => v.clienttypename != "");

                foreach (var stock in query)
                    my.Add(stock.clienttypename);

                // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return my;
        }
        private List<string> Sectors(string path)
        {
            List<string> my = new List<string>();
            try
            {
                var db = new SQLiteConnection(path);
                // this counts all records in the database, it can be slow depending on the size of the database
                ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                var query = db.Table<Sector>().Where(v => v.sectorname != "");

                foreach (var stock in query)
                    my.Add(stock.sectorname);

                // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return my;
        }
        private List<string> Branches(string path,string nm)
        {
            List<string> my = new List<string>();
            try
            {
                if (nm != "--Select Bank--")
                {
                    var db = new SQLiteConnection(path);
                    // this counts all records in the database, it can be slow depending on the size of the database
                    ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                    var query = db.Table<Branch>().Where(v => v.branchname != "");

                    foreach (var stock in query)
                        my.Add(stock.branchname);
                }
                // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return my;
        }

        private string BranchesC(string path, string nm,string pc)
        {
            string uv = "";
            try
            {
                if (nm != "--Select Branch--")
                {
                    var db = new SQLiteConnection(path);
                    // this counts all records in the database, it can be slow depending on the size of the database
                    ///var count = db.ExecuteScalar<List<string>>("SELECT bankname FROM Bank");
                    var query = db.Table<Branch>().Where(v => v.branchname==nm.ToLower().Replace(" ", "") && v.branchcode.ToLower().Replace(" ","")==pc.ToLower().Replace(" ", ""));

                    foreach (var stock in query)
                        uv = stock.branchcode;
                }
                // return my;
            }
            catch (SQLiteException ex)
            {
                return null;
            }
            return uv;
        }

        private void _dateSelectButton_Click(object sender, System.EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        
    }
}