using Android;

using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using OldMutualAndroid.Model;
using System;
using System.Collections.Generic;
using TextDrawable;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;
using CMS.Model;
using Android.Support.V4.App;
using CMS.Activities;
using static Android.Widget.TabHost;

namespace CMS.Fragments
{
    public class Loans : Fragment
    {

        private List<LoanStatus> mItems;
        //private List<LoanBalance> mItems2;
        private ListView mListView;
       // private ListView mListView2;
        CustomList adapter;
        CustomListB adapter2;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Loans NewInstance()
        {
            var frag1 = new Loans { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

           View view= inflater.Inflate(Resource.Layout.Loans, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("LOAN TRACKER");
            spec1.SetContent(Resource.Id.tab1);
            spec1.SetIndicator("LOAN TRACKER");

              
         
            tabHost.AddTab(spec1);
            try
            {
                var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("LoanStatus/{s}", Method.GET);
                request.AddUrlSegment("s", MainActivity.membernumber);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                IRestResponse response = client.Execute(request);
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<LoanStatus> dataList = JsonConvert.DeserializeObject<List<LoanStatus>>(validate);

                var dbsel = from s in dataList
                            select s;

                mItems = new List<LoanStatus>();

                foreach (var k in dbsel)
                {
                    mItems.Add(new LoanStatus() { ID = k.ID, STATUS = k.STATUS, AMOUNT = Convert.ToDecimal(k.AMOUNT), CREATED_DATE=k.CREATED_DATE });

                }
                // mItems.Add(new LoanStatus() { ID = "235", STATUS = "DISBURSED", AMOUNT = Convert.ToDecimal(8.504) });
                mListView = view.FindViewById<ListView>(Resource.Id.listView1);
                //mListView2 = view.FindViewById<ListView>(Resource.Id.listView2);

                Drawable[] imageId = new Drawable[mItems.Count];
                int x = 0;
                TextDrawable.TextDrawable drawable;
                foreach (var d in mItems)
                {

                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    try
                    {
                        drawable = TextDrawable.TextDrawable.TextDrawbleBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRound(d.STATUS.Substring(0, 1), GetRandomColor());
                       
                    }
                    catch (Exception)
                    {

                        continue;
                    }
                   imageId[x] = drawable;
                    x += 1;

                }


                //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());
                adapter = new CustomList(this.Activity, mItems, imageId);
                mListView.Adapter = adapter;

            }
            catch (Exception f)
            {

                string msg = f.Message;
           
            }
            //list 2
    
            mListView.ItemLongClick += MListView_ItemLongClick;
            //mListView2.ItemLongClick += MListView2_ItemLongClick;
            return view;
        }

        private void MListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            string data = adapter.GetItemAtPositionId(e.Position).ToString();

            string p = data;
            MainActivity.loanno = p;
            //get loan balance
            var client = new RestClient(MainActivity.baseUrl);
            var request = new RestRequest("Balances/{s}", Method.GET);
            request.AddUrlSegment("s", MainActivity.membernumber);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<LoanBalance> dataList = JsonConvert.DeserializeObject<List<LoanBalance>>(validate);

            var dbsel = from j in dataList
                        where j.Loan.Split(' ')[2] == MainActivity.loanno
                        select j;
            foreach (var f in dbsel)
            {

                MainActivity.loanbalance = Convert.ToDecimal(f.Balance);
            }

            // FragmentTransaction trans = this.FragmentManager.BeginTransaction();
            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, LoanStat.NewInstance(), "Fragment3");
            trans2.AddToBackStack("Fragment3");
            trans2.Commit();
            //var menu = new PopupMenu(this.Activity, mListView.GetChildAt(e.Position));
            //menu.Inflate(Resource.Layout.popmenu);
            //menu.MenuItemClick += (s, a) =>
            //{
            //    switch (a.Item.ItemId)
            //    {
            //        case Resource.Id.pop_button1:
            //            {

            //            }
            //            break;

            //        default:
            //            break;
            //    }
            //};
            //menu.Show();
        }

        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}