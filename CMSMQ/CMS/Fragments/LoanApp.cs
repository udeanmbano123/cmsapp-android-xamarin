﻿using Android;

using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using OldMutualAndroid.Model;
using System;
using System.Collections.Generic;
using TextDrawable;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;
using CMS.Model;
using Android.Support.V4.App;
using CMS.Activities;
using static Android.Widget.TabHost;
using SQLite;

namespace CMS.Fragments
{
    public class LoanApp : Fragment
    {
        public EditText member,Gname, GIdNo, Gphone, Grelationship, GAddress,loanamt,recamt,sourcrep;
        public TextView Dbirth,repadt,appdt,empdate;
        public EditText curremployer, empaddr, suprvme, empphone, empPosition, gross, netsal, phoneno;
        private List<string> product = new List<string>();
        Spinner products;
        private List<string> purpose = new List<string>();
        Spinner purposes;
        TextView _dateDisplay;
        Button _dateSelectButton;
        TextView _dateDisplay2;
        Button _dateSelectButton2;
        TextView _dateDisplay3;
        Button _dateSelectButton4;
        TextView _dateDisplay4;
        Button _dateSelectButton3;
        Button submit;
        // Create your fragment here
        public string path;
        public static LoanApp NewInstance()
        {
            var frag1 = new LoanApp{ Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            path = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CMSM.db3");

            View view = inflater.Inflate(Resource.Layout.Appl, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Guarantor");
            spec1.SetIndicator("Guarantor Information");
            spec1.SetContent(Resource.Id.tab1);


            TabSpec spec2 = tabHost.NewTabSpec("Loan");
            spec2.SetIndicator("Loan Parameters");
            spec2.SetContent(Resource.Id.tab2);

            TabSpec spec3 = tabHost.NewTabSpec("Employment");
            spec3.SetIndicator("Employment Information");
            spec3.SetContent(Resource.Id.tab3);
;

            tabHost.AddTab(spec1);
            tabHost.AddTab(spec2);
            tabHost.AddTab(spec3);
            /* public EditText Gname, GIdNo, Gphone, Grelationship, GAddress, loanamt, recamt, sourcrep;
         public TextView Dbirth, repadt, appdt;
     */
            //Guarantor
            Gname=(EditText)view.FindViewById(Resource.Id.names);
            GIdNo = (EditText)view.FindViewById(Resource.Id.idno);
            Gphone = (EditText)view.FindViewById(Resource.Id.phone);
            Grelationship = (EditText)view.FindViewById(Resource.Id.rele);
            GAddress = (EditText)view.FindViewById(Resource.Id.curr);
            loanamt = (EditText)view.FindViewById(Resource.Id.lamt);
            recamt = (EditText)view.FindViewById(Resource.Id.lramt);
            sourcrep = (EditText)view.FindViewById(Resource.Id.srcamt);
            //employee
            curremployer = (EditText)view.FindViewById(Resource.Id.curremployee);
            empaddr = (EditText)view.FindViewById(Resource.Id.empaddress);
            suprvme = (EditText)view.FindViewById(Resource.Id.supervisor);
            empphone = (EditText)view.FindViewById(Resource.Id.phone);
            empPosition = (EditText)view.FindViewById(Resource.Id.empPosition);
            gross = (EditText)view.FindViewById(Resource.Id.grosssalary);
           netsal = (EditText)view.FindViewById(Resource.Id.netsalary);
            Dbirth = (TextView)view.FindViewById(Resource.Id.dateDisplay);
             repadt = (TextView)view.FindViewById(Resource.Id.dateDisplay2);
            appdt = (TextView)view.FindViewById(Resource.Id.dateDisplay3);
            empdate = (TextView)view.FindViewById(Resource.Id.dateDisplay4);
            _dateDisplay = view.FindViewById<TextView>(Resource.Id.dateDisplay);
            _dateSelectButton = view.FindViewById<Button>(Resource.Id.dateSelectButton);
            _dateSelectButton.Click += _dateSelectButton_Click;
            _dateDisplay2 = view.FindViewById<TextView>(Resource.Id.dateDisplay2);
            _dateSelectButton2 = view.FindViewById<Button>(Resource.Id.dateSelectButton2);
            _dateSelectButton2.Click += _dateSelectButton2_Click;
            _dateDisplay3 = view.FindViewById<TextView>(Resource.Id.dateDisplay3);
            _dateSelectButton3 = view.FindViewById<Button>(Resource.Id.dateSelectButton3);
            _dateSelectButton3.Click += _dateSelectButton3_Click;
            _dateDisplay4 = view.FindViewById<TextView>(Resource.Id.dateDisplay4);
            _dateSelectButton4 = view.FindViewById<Button>(Resource.Id.dateSelectButton4);
            _dateSelectButton4.Click += _dateSelectButton4_Click;
            submit = (Button)view.FindViewById(Resource.Id.btnLogin);
              products=(Spinner)view.FindViewById(Resource.Id.product);
             purposes=(Spinner)view.FindViewById(Resource.Id.purposespin);
            member= (EditText)view.FindViewById(Resource.Id.member);
            member.Enabled = false;
            member.Text = MainActivity.membernumber;
            submit.Click += Submit_Click;
            product.Add("--Select Product--");
            product.Add("Business 1"); 
            product.Add("Good Clients");
            var adapter4 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, product);

            products.Adapter = adapter4;
            purpose.Add("--Select Purpose--");
            purpose.Add("Educational");
            purpose.Add("Agricultural");
            
            var adapter5 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, purpose);

            purposes.Adapter = adapter5;
            return view;
        }

        private void _dateSelectButton4_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay4.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);

        }

        private void Submit_Click(object sender, EventArgs e)
        {
            //submit
             if (products.SelectedItem.ToString()== "--Select Product--")
            {

                Toast.MakeText(this.Activity, "Select product is required", ToastLength.Long).Show();

            }
            else if (loanamt.Text=="")
            {

                Toast.MakeText(this.Activity, "Loan amount is required", ToastLength.Long).Show();

            }
            else if (recamt.Text=="")
            {

                Toast.MakeText(this.Activity, "Recommended amount is required", ToastLength.Long).Show();

            }
            else if (_dateDisplay2.Text=="")
            {

                Toast.MakeText(this.Activity, "Application date is required", ToastLength.Long).Show();

            }
            else if (_dateDisplay3.Text == "")
            {

                Toast.MakeText(this.Activity, "Repayment date is required", ToastLength.Long).Show();

            }else
            {
                Model.Loans my = new Model.Loans();
                my.membernumber =member.Text;
                //guarantor
                my.name = Gname.Text;
                my.IDNo = GIdNo.Text;
                my.PhoneNo = Gphone.Text;
                my.Relationship = Grelationship.Text;
                my.Address = GAddress.Text;
                /* public EditText Gname, GIdNo, Gphone, Grelationship, GAddress, loanamt, recamt, sourcrep;
                public TextView Dbirth, repadt, appdt;
            */

                try
                {
                    my.dob = Convert.ToDateTime(Dbirth.Text);
                }
                catch (Exception)
                {

                    my.dob = Convert.ToDateTime(null);
                }

                //loan
                my.product = products.SelectedItem.ToString();
                my.loanamounty = Convert.ToDecimal(loanamt.Text);

                my.recamount = Convert.ToDecimal(recamt.Text);

                my.repaysource = sourcrep.Text;

                my.loanpurpose = purposes.SelectedItem.ToString();

                my.frepay = Convert.ToDateTime(repadt.Text);

                my.appdate= Convert.ToDateTime(appdt.Text);
                //employment information
                my.currentemp = curremployer.Text;
                my.empaddress = empaddr.Text;
                try 
	{	        
		my.dateemp = Convert.ToDateTime(empdate.Text);
	}
	catch (Exception )
	{

                    my.dateemp = Convert.ToDateTime(null);
                }
                my.Supervisor = suprvme.Text;
                my.Phone = empphone.Text;
                my.Position = empPosition.Text;
                my.grosssal = Convert.ToDecimal(gross.Text);
                my.netsalary = Convert.ToDecimal(netsal.Text);
                
                my.username = MainActivity.username;

                my.refrr = "NEW";

                  my.dateCreated = DateTime.Now.ToString();

                my.status = "NEW";
                string app = insertUpdateData(my,path);

                if (app == "Inserted")
                {
                    Toast.MakeText(this.Activity, "Loan application created successfully", ToastLength.Long).Show();
                    var trans2 = this.FragmentManager.BeginTransaction();
                    trans2.Replace(Resource.Id.content_frame, CustomersLL.NewInstance(), "LoanApp");
                    trans2.AddToBackStack("LoanApp");
                    trans2.Commit();
                }
                else
                {
                    Toast.MakeText(this.Activity, "Loan application not created successfully", ToastLength.Long).Show();

                }
            }
           

        }

        private void _dateSelectButton3_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay3.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private void _dateSelectButton2_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay2.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private void _dateSelectButton_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }
        private string insertUpdateData(Model.Loans data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
    }
}
 