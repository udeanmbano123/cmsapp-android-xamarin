﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CMS.Intents
{
    [Service]
    public class OfficerIntent : IntentService
    {
        public OfficerIntent() : base("OfficerIntent")
    {
        Console.WriteLine("perform some long running work");
        ..
        Console.WriteLine("work complete");
        }

        protected override void OnHandleIntent(Android.Content.Intent intent)
        {
            //sync Officer data
            Console.WriteLine("perform some long running work");
      
        Console.WriteLine("work complete");
        }
    }
}