﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace CMS.Model
{
   public  class Background
    {

      public string insertUpdateDataB(Bank data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
      public string insertUpdateDataBr(Branch data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
      public string insertUpdateDataS(Sector data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

      public string insertUpdateDataC(clienttype data, string path)
        {
            try
            {
                var db = new SQLiteAsyncConnection(path);
                db.InsertAsync(data);
                return "Inserted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
    }
}