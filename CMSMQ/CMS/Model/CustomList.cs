using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using CMS.Model;
using CMS;

namespace OldMutualAndroid.Model
{
    class CustomList : BaseAdapter
    {
        private Activity context;
        public List<LoanStatus> listitem;
      
        private Drawable[] imageId;
        //public override int Count
        //{
        //    get
        //    {
        //        return listitem.Count;
        //    }
        //}
    
        public CustomList(Activity context, List<LoanStatus> listitem,Drawable[] imageId)
        {
            this.context = context;
            this.listitem = listitem;
  

            this.imageId = imageId;
        }
        public override int Count
        {
            get { return listitem.Count; }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public LoanStatus GetItemAtPosition(int position)
        {
            return listitem[position];
        }

        public String GetItemAtPositionId(int position)
        {
            return listitem[position].ID.ToString();
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listitem[position];
            var view = context.LayoutInflater.Inflate(Resource.Layout.list_single, parent, false);
            TextView txtTitle = (TextView)view.FindViewById(Resource.Id.txt4);
            TextView txtStatus= (TextView)view.FindViewById(Resource.Id.txt2);
            TextView txtInstallment = (TextView)view.FindViewById(Resource.Id.txt3);
            ImageView imageView = (ImageView)view.FindViewById(Resource.Id.img);
            txtTitle.SetText("Loan ID : "+item.ID +","+ Environment.NewLine +"Loan Date :"+ item.CREATED_DATE.ToString("dd-MMM-yyyy"), TextView.BufferType.Normal);
            txtStatus.SetText(item.STATUS, TextView.BufferType.Normal);
            txtInstallment.SetText(item.AMOUNT.ToString("#,##00.00"), TextView.BufferType.Normal);
            //txtTitle.Text = (listitem[position]).ToString();
            //txtDate.Text = (listitem[position]).ToString();
            imageView.SetImageDrawable(imageId[position]);
            return view;
        }
    }
}