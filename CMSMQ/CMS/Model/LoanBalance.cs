﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CMS.Model
{
    public class LoanBalance
    {
       public string Loan { get; set; } 
       public decimal Balance { get; set; }
    }
}