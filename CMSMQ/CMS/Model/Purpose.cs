﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace CMS.Model
{
   public class Purpose
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        [Unique]
        public string purposeN { get; set; }
    }
}