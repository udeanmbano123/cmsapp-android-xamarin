using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using static Android.Widget.TabHost;

namespace CMS.Fragments
{
    public class Fragment2 : Fragment
    {
        private List<string> clientypes = new List<string>();
        Spinner clients;
        private List<string> bank = new List<string>();
        Spinner banks;
        private List<string> branch = new List<string>();
        Spinner branchs;
        private List<string> sector = new List<string>();
        Spinner sectors;

        TextView _dateDisplay;
       Button _dateSelectButton;
        TextView _dateDisplay2;
        Button _dateSelectButton2;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Fragment2 NewInstance()
        {
            var frag2 = new Fragment2 { Arguments = new Bundle() };
            return frag2;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

                 View view= inflater.Inflate(Resource.Layout.Statics, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Static Details");
            spec1.SetContent(Resource.Id.tab1);
            spec1.SetIndicator("Static Details");

            TabSpec spec2 = tabHost.NewTabSpec("Spouse Details");
            spec2.SetIndicator("Spouse Details");
            spec2.SetContent(Resource.Id.tab2);

            TabSpec spec3 = tabHost.NewTabSpec("Bank Details");
            spec3.SetIndicator("Bank Details");
            spec3.SetContent(Resource.Id.tab3);

            TabSpec spec4 = tabHost.NewTabSpec("Employment Information");
            spec4.SetIndicator("Employment Information");
            spec4.SetContent(Resource.Id.tab4);

            tabHost.AddTab(spec1);
            tabHost.AddTab(spec2);
            tabHost.AddTab(spec3);
            tabHost.AddTab(spec4);

            //binding lists
           clients=(Spinner)view.FindViewById(Resource.Id.sbu);
         banks = (Spinner)view.FindViewById(Resource.Id.drpbank);
            branchs = (Spinner)view.FindViewById(Resource.Id.drpbranch);
            sectors = (Spinner)view.FindViewById(Resource.Id.drpsector);
          
            clientypes.Add("--Select Client Type--");
            bank.Add("--Select Bank--");
            branch.Add("--Select Branch--");
            sector.Add("--Select Sector--");

            var adapter1 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, clientypes);

            clients.Adapter = adapter1;

            var adapter2 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, bank);

            banks.Adapter = adapter2;

            var adapter3= new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, branch);

            branchs.Adapter = adapter3;
            var adapter4 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, sector);

            sectors.Adapter = adapter4;

            _dateDisplay = view.FindViewById<TextView>(Resource.Id.dateDisplay);
            _dateSelectButton = view.FindViewById<Button>(Resource.Id.dateSelectButton);
            _dateSelectButton.Click += _dateSelectButton_Click;
            _dateDisplay2 = view.FindViewById<TextView>(Resource.Id.dateDisplay2);
            _dateSelectButton2 = view.FindViewById<Button>(Resource.Id.dateSelectButton2);
            _dateSelectButton2.Click += _dateSelectButton2_Click;
            return view;
        }

        private void _dateSelectButton2_Click(object sender, System.EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay2.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private void _dateSelectButton_Click(object sender, System.EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay.Text = time.ToLongDateString();
            });
            frag.Show(this.Activity.FragmentManager, DatePickerFragment.TAG);
        }

        
    }
}