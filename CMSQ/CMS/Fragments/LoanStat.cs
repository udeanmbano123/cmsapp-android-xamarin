using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using CMS.Model;
using OldMutualAndroid.Model;
using System;
using System.Collections.Generic;
using static Android.Widget.TabHost;
using CMS.Activities;
using RestSharp;
using Newtonsoft.Json;
using System.Linq;

namespace CMS.Fragments
{
    public class LoanStat : Fragment
    {
        private List<LoanStatement> mItems;
         private ListView mListView;
        private ListView mListView2;
        CustomListS  adapter;
        TextView bal;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static LoanStat NewInstance()
        {
            var frag1 = new LoanStat { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.LoanStatement, null);
            TabHost tabHost;
            tabHost = (TabHost)view.FindViewById(Resource.Id.tabHost);
            tabHost.Setup();

            TabSpec spec1 = tabHost.NewTabSpec("Loan Statement Summary");
            spec1.SetContent(Resource.Id.tab1);
            spec1.SetIndicator("Loan Statement Summary");

           tabHost.AddTab(spec1);
            //loanbalance

            mListView = view.FindViewById<ListView>(Resource.Id.listView1);
            bal= view.FindViewById<TextView>(Resource.Id.txt2);

            bal.Text = "BALANCE $" + MainActivity.loanbalance.ToString("#,##00.00");
            mItems = new List<LoanStatement>();
            try
            {
                var client = new RestClient(MainActivity.baseUrl);
                var request = new RestRequest("Statement/{s}/{p}", Method.GET);
                request.AddUrlSegment("s", MainActivity.membernumber.ToString().Replace("/", "-"));
                request.AddUrlSegment("p", MainActivity.loanno);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                IRestResponse response = client.Execute(request);
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<LoanStatement> dataList = JsonConvert.DeserializeObject<List<LoanStatement>>(validate);

                var dbsel = from j in dataList

                            select j;
                decimal bals = 0;
                decimal bals22 = 0;
                decimal repay = 0;
                int count =0;
                foreach (var c in dbsel)
                {
                    if (c.Description == "Interest to Maturity" || c.Description == "Disbursement")
                    {
                        bals22 += Convert.ToDecimal(c.Amount);
                    }

                    if (c.Description!="Interest to Maturity" || c.Description != "Disbursement")
                    {
                        repay += Convert.ToDecimal(c.Amount);
                    }
                    bals += Convert.ToDecimal(c.Amount);
                    //mItems.Add(new LoanStatement() { TrxnDate = Convert.ToDateTime(c.TrxnDate), Amount = Convert.ToDecimal(c.Amount), Refrence = c.Refrence, Description = c.Description, Balance = Convert.ToDecimal(bals) });
                    count++;
                    if (count ==dbsel.Count())
                    {
                        mItems.Add(new LoanStatement() { TrxnDate = Convert.ToDateTime(c.TrxnDate), Amount = Convert.ToDecimal(bals22), Refrence = c.Refrence, Description = "Disbursed Principle + Interest to Maturity -"+ (count-2).ToString() +" Repayments", Balance = Convert.ToDecimal(bals) });

                    }
                }

                Drawable[] imageId = new Drawable[mItems.Count];
                int x = 0;
                TextDrawable.TextDrawable drawable;
                foreach (var d in mItems)
                {

                    try
                    {
                        //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                         drawable = TextDrawable.TextDrawable.TextDrawbleBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRound(d.TrxnDate.ToString("MMM-yyyy"), GetRandomColor());
                        imageId[x] = drawable;
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    x += 1;
                }


                //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());
                adapter = new CustomListS(this.Activity, mItems, imageId);
                mListView.Adapter = adapter;

            }
            catch (Exception)
            {

              
            } return view;
        }

        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}