using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using CMS.Model;
using CMS;

namespace OldMutualAndroid.Model
{
    class CustomListB : BaseAdapter
    {
        private Activity context;
        public List<LoanBalance> listitem;
      
        private Drawable[] imageId;
        //public override int Count
        //{
        //    get
        //    {
        //        return listitem.Count;
        //    }
        //}
    
        public CustomListB(Activity context, List<LoanBalance> listitem,Drawable[] imageId)
        {
            this.context = context;
            this.listitem = listitem;
  

            this.imageId = imageId;
        }
        public override int Count
        {
            get { return listitem.Count; }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public LoanBalance GetItemAtPosition(int position)
        {
            return listitem[position];
        }

        public String GetItemAtPositionId(int position)
        {
            return listitem[position].Loan.ToString();
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listitem[position];
            var view = context.LayoutInflater.Inflate(Resource.Layout.list_singleB, parent, false);
            TextView txtTitle = (TextView)view.FindViewById(Resource.Id.txt4);
            TextView txtStatus= (TextView)view.FindViewById(Resource.Id.txt2);
     
            ImageView imageView = (ImageView)view.FindViewById(Resource.Id.img);
            txtTitle.SetText(item.Loan.ToUpper(), TextView.BufferType.Normal);
            txtStatus.SetText("LOAN BALANCE $"+item.Balance, TextView.BufferType.Normal);
           //txtTitle.Text = (listitem[position]).ToString();
            //txtDate.Text = (listitem[position]).ToString();
            imageView.SetImageDrawable(imageId[position]);
            return view;
        }
    }
}